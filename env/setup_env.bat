@ECHO OFF

set root_conda=%userprofile%\anaconda3

set root_code=%~dp0%..

call %root_conda%\Scripts\activate.bat %root%

call conda create -n pdrop python=3.7.7 -y

call conda activate pdrop

call pip install -r %root_code%\env\requisitos_ambiente.txt


pause