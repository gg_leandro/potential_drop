# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 12:22:58 2020

@author: GG
"""
import numpy as np
import pandas as pd
import pickle
from scipy.interpolate import griddata
import pickle
import os
from copy import copy


def gera_ref_sem_defeito(endereco,x,y):
	
	grad_sem_defeito = np.array(pd.read_csv(endereco,header = 0))
	[xx,yy] = np.meshgrid(x,y)
	interpolado = griddata(grad_sem_defeito[:,0:2],grad_sem_defeito[:,2],(xx,yy),method = 'linear')
	interpolado = np.flip(interpolado,0) # É preciso rotacionar verticalmente a imagem

	with open(os.path.join(os.path.split(endereco)[0],"ref_sem_defeito.pickle"), 'wb') as f:
		pickle.dump(interpolado, f)

	return interpolado


def txt_matlab_comsol_2py(endereco):

	arquivo = []
	with open(endereco) as fd:
		lines = fd.readlines()
	 
	for item in lines:
		strs = item.strip('\n')
		if not "%" in strs: # Rejeita o cabeçalho
			arquivo.append((strs.split())) # Retira os mútiplos espaços
	
	return np.array(arquivo)

def interpola_grad(grad_modulo,x,y):
	
	[xx,yy] = np.meshgrid(x,y)
	interpolado = griddata(grad_modulo[:,0:2],grad_modulo[:,2],(xx,yy),method = 'linear',fill_value=1)
	interpolado = np.flip(interpolado,0) # É preciso rotacionar verticalmente a imagem

	return interpolado

def calculo_distancia(x1,y1,x2,y2):
	distancia = np.sqrt((x1 - x2)**2 + (y1 - y2)**2)
	
	return np.round(distancia,3)

def cria_pasta(endereco_destino):
	if not os.path.exists(endereco_destino):
		os.makedirs(endereco_destino)

def correlation(dataset, threshold):
	from copy import copy
	dataset = copy(dataset)
	col_corr = set() # Set of all the names of deleted columns
	corr_matrix = dataset.corr()
	for i in range(len(corr_matrix.columns)):
		for j in range(i):
			if (abs(corr_matrix.iloc[i, j]) > threshold) and (corr_matrix.columns[j] not in col_corr):
				colname = corr_matrix.columns[i] # getting the name of column
				col_corr.add(colname)
				if colname in dataset.columns:
					del dataset[colname] # deleting the column from the dataset
	return(dataset,col_corr)


def histograma(data, edges, ax):
	import matplotlib.pyplot as plt
	import numpy as np
  
	# Colours for different percentiles
	perc_25_colour = 'gold'
	perc_50_colour = 'mediumaquamarine'
	perc_75_colour = 'mediumaquamarine'#'deepskyblue'
	perc_95_colour = 'peachpuff'
	   
	'''
	counts  = numpy.ndarray of count of data ponts for each bin/column in the histogram
	bins    = numpy.ndarray of bin edge/range values
	patches = a list of Patch objects.
			each Patch object contains a Rectangle object. 
			e.g. Rectangle(xy=(-2.51953, 0), width=0.501013, height=3, angle=0)
	'''
	counts, bins, patches = ax.hist(data, facecolor='red', edgecolor='gray',bins=edges)   
	
	# Set the ticks to be at the edges of the bins.
	ax.set_xticks(bins.round(2))
	plt.xticks(rotation=70)
	plt.tight_layout()
	# Change the colors of bars at the edges
	twentyfifth, seventyfifth, ninetyfifth = np.percentile(data, [25, 50, 75])
			   
	# Calculate bar centre to display the count of data points and %
	bin_x_centers = 0.7 * np.diff(bins) + bins[:-1]
	bin_y_centers = ax.get_yticks()[1] * 0.35
   
	# Display the the count of data points and % for each bar in histogram
	for i in range(len(bins)-1):
		# bin_label = "{0:,}".format(counts[i]) + "  ({0:,.2f}%)".format((counts[i]/counts.sum())*100)
		porcentagem = (counts[i]/counts.sum())*100
		if porcentagem > 1:
			bin_label = "  {0:,.2f}%".format(porcentagem)
			plt.text(bin_x_centers[i], bin_y_centers, bin_label, rotation=90, rotation_mode='anchor',fontsize = 11)           

			if porcentagem < 10:
				patches[i].set_facecolor(perc_25_colour)
			if porcentagem > 10 and porcentagem < 50:
				patches[i].set_facecolor(perc_75_colour)
			if porcentagem > 50:
				patches[i].set_facecolor(perc_50_colour)          
	plt.show()


def gera_csv(nome_csv, area_classe, porcentagem):
	texto = [];formato = []
	for i in range(len(porcentagem)):
		texto.append("area" + str(i+1) + ",")
		formato.append('%i,')

	texto = "".join(texto) # Retira as apas
	texto = texto + "raio_seg,assinatura,area_real,prof_real,classe"
	formato = "".join(formato)
	formato = formato + "%.8f,%.8f,%.4f,%.4f,%i"

	try:    
		col = max(len(max(area_classe[0][0][:],key = lambda x: len(x))),len(max(area_classe[0][0][:],key = lambda x: len(x))))
		
		num_linhas0 = 0
		for k in range(0,len(area_classe[0][:][:][:])):
			num_linhas0 = num_linhas0 + len(area_classe[0][k][:][:])
			
		area_classe_mat0 = np.zeros([num_linhas0,col])
		
		temp = 0;indice = 0;temp1 = 0
		while temp1 < num_linhas0 and indice < len(area_classe[0][:][:][:])+1:        
			temp += len(area_classe[0][indice][:][:])
			area_classe_mat0[temp1:temp] = np.array(area_classe[0][indice][:])
			
			temp1 = copy(temp)
			indice += 1
			
		area_classe_final = np.zeros([num_linhas0,col])  
		area_classe_final[0:num_linhas0][:] = area_classe_mat0
				
		np.savetxt(nome_csv, area_classe_final, delimiter=",", comments='', fmt = formato, header = texto)
	except Exception:
		print("Nenhum defeito encontrado")  