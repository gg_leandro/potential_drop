function out = gera_modelo_quadrado(parametros,endereco,defeito,salva)

% Model exported on Jun 17 2019, 14:17 by COMSOL 5.4.0.225.
% Grande atualiza��o em 15/05/2020
    % Esta fun��o permite realizar simula��es de placas com ou sem defeitos
    %   Os par�metros de entrada s�o:
    %        "parametros" traz a parametriza��o usada na simula��o, por ex:
                    % parametros = ones(1,14);
                    % parametros(1) = 30; % comprimento da pe�a (cm)
                    % parametros(2) = 30; % largura da pe�a (cm)
                    % parametros(3) = 10; % espessura da pe�a (mm)
                    % parametros(4) = 3; % dist�ncia do eletrodo � borda, coordenada em x (cm) % Sem uso
                    % parametros(5) = 0; % altura do eletrodo, coordenada em y (cm)
                    % parametros(6) = .6; % di�metro do contato (mm)
                    % parametros(7) = .2; % comprimento do eletrodo (mm)
                    % parametros(8) = 30; % temperatura (�C)
                    % parametros(9) = 10; % corrente (A)
                    % parametros(10) = 1E-5; % toler�ncia no c�lculo
                    % parametros(11) = 35; % press�o de contato (MPa)
                    % parametros(12) = 1; % afastamento da zona de perturba��o, na vizinhan�a dos eletrodos (cm)
                    % parametros(13) = -12; % x_eletrodo_pos
                    % parametros(14) = 12; % x_eletrodo_neg 
    %        "endere�o" informa o local onde os dados resultantes ser�o salvos
    %        "defeito" informa os par�metros, em mm, do(s)defeito(s) a ser(em) simulado(s), por ex:
                    % 22.26,2.6618749156283,1.5,-30.08,-10
                    % 11.46,1.90992965725606,24.82,5.07,-10
                    % 16.99,2.32552896482985,15,-10.9,-10
                    % 7.78,1.57367433559485,-32.5,-6.65,-10
                    % 2.92,0.964087582980234,-9.72,22.58,-10

    %        Se defeito = [] ser� simulada uma placa lisa, sem defeitos
    %        "salva" permite escolher quais dados ser�o salvos
    %        Se: salva = 1; ser� salvo em txt o m�dulo do gradiente de potencial na superf�cie de toda a placa 
    %            salva = 0; ser� salvo em txt as componentes (x e y) do gradiente de potencial na superf�cie de toda a placa 
    %        Deve ser inserido model.result.export('plot_tag').run; em cada conjunto
    %        de dados a ser exportado

import com.comsol.model.*
import com.comsol.model.util.*

ModelUtil.showProgress(false);
model = ModelUtil.create('Model');

model.label('geometria_quadrada_esferas.mph');
cprintf('blue','Associando par�metros\n')
model.param.set('b2', strcat(num2str(parametros(1)),'[cm]'), ['largura da pe' native2unicode(hex2dec({'00' 'e7'}), 'unicode') 'a']);
model.param.set('h2', strcat(num2str(parametros(2)),'[cm]'), ['altura da pe' native2unicode(hex2dec({'00' 'e7'}), 'unicode') 'a']);
model.param.set('p2', strcat(num2str(parametros(3)),'[mm]'), ['espessura da pe' native2unicode(hex2dec({'00' 'e7'}), 'unicode') 'a']);
model.param.set('dist_borda', strcat(num2str(parametros(4)),'[cm]'), 'coordenada em x');
model.param.set('y_eletrodo', strcat(num2str(parametros(5)),'[cm]'), 'coordenada em y');
model.param.set('diam_contato', strcat(num2str(parametros(6)),'[mm]'));
model.param.set('comp_eletrodo', strcat(num2str(parametros(7)),'[cm]'));
model.param.set('temp', strcat(num2str(parametros(8)),'[degC]'));
model.param.set('corrente', strcat(num2str(parametros(9)),'[A]'));
model.param.set('erro', num2str(parametros(10)));
model.param.set('pressao_contato',strcat(num2str(parametros(11)),'[MPa]'));
model.param.set('afastamento_elet',strcat(num2str(parametros(12)),'[cm]'));
model.param.set('x_eletrodo_pos', strcat(num2str(parametros(13)),'[cm]'));
model.param.set('x_eletrodo_neg', strcat(num2str(parametros(14)),'[cm]'));

if ~isempty(defeito) % Se n�o for informado algum defeito, ser� simulada uma placa sem danos
    [num_esf,~] = size(defeito);
    raio_esf = defeito(:,2);
    x_esf = defeito(:,3);
    y_esf = defeito(:,4);
    z_esf = defeito(:,5);

    raio = {num_esf};x = {num_esf};y = {num_esf};z = {num_esf};
    for i = 1:num_esf % gera��o do conjunto de par�metros para cada esfera
        raio{i} = strcat('r',num2str(i));
        model.param.set(raio{i}, strcat(num2str(raio_esf(i)),'[mm]'),strcat('raio esfera',num2str(i)));
        x{i} = strcat('x',num2str(i));
        model.param.set(x{i}, strcat(num2str(x_esf(i)),'[mm]'),strcat('coord x esfera',num2str(i)));
        y{i} = strcat('y',num2str(i));
        model.param.set(y{i}, strcat(num2str(y_esf(i)),'[mm]'),strcat('coord y esfera',num2str(i)));
        z{i} = strcat('z',num2str(i));
        model.param.set(z{i}, strcat(num2str(z_esf(i)),'[mm]'),strcat('coord z esfera',num2str(i)));
    end
end

model.component.create('comp1', false);

model.component('comp1').geom.create('geom1', 3);

model.component('comp1').comments('\n');

model.component('comp1').mesh.create('mesh1');

cprintf('blue','Criando geometria\n')
model.component('comp1').geom('geom1').lengthUnit('cm');
model.component('comp1').geom('geom1').repairTolType('relative');
model.component('comp1').geom('geom1').create('blk1', 'Block');
model.component('comp1').geom('geom1').feature('blk1').label('Placa');
model.component('comp1').geom('geom1').feature('blk1').set('pos', {'0' '0' '-p2/2'});
model.component('comp1').geom('geom1').feature('blk1').set('base', 'center');
model.component('comp1').geom('geom1').feature('blk1').set('size', {'b2' 'h2' 'p2'});
model.component('comp1').geom('geom1').create('cyl1', 'Cylinder');
model.component('comp1').geom('geom1').feature('cyl1').label('Eletrodo+');
model.component('comp1').geom('geom1').feature('cyl1').set('pos', {'x_eletrodo_pos' 'y_eletrodo' '0'});
model.component('comp1').geom('geom1').feature('cyl1').set('r', 'diam_contato/2');
model.component('comp1').geom('geom1').feature('cyl1').set('h', 'comp_eletrodo');
model.component('comp1').geom('geom1').create('cyl2', 'Cylinder');
model.component('comp1').geom('geom1').feature('cyl2').label('Eletrodo-');
model.component('comp1').geom('geom1').feature('cyl2').set('pos', {'x_eletrodo_neg' 'y_eletrodo' '0'});
model.component('comp1').geom('geom1').feature('cyl2').set('r', 'diam_contato/2');
model.component('comp1').geom('geom1').feature('cyl2').set('h', 'comp_eletrodo');

if ~isempty(defeito)
    rotulo = {num_esf};
    for i = 1:num_esf % gera��o das esferas 
        rotulo{i} = strcat('sph',num2str(i));
        model.component('comp1').geom('geom1').create(rotulo{i}, 'Sphere');
        model.component('comp1').geom('geom1').feature(rotulo{i}).set('pos', {x{i} y{i} z{i}});
        model.component('comp1').geom('geom1').feature(rotulo{i}).set('r', raio{i});    
    end
    model.component('comp1').geom('geom1').create('dif1', 'Difference');
    model.component('comp1').geom('geom1').feature('dif1').set('intbnd', false);
    model.component('comp1').geom('geom1').feature('dif1').selection('input').set({'blk1'});
    model.component('comp1').geom('geom1').feature('dif1').selection('input2').set(rotulo);

    model.component('comp1').geom('geom1').create('uni1', 'Union');
    model.component('comp1').geom('geom1').feature('uni1').selection('input').set({'dif1'});
end

model.component('comp1').geom('geom1').feature('fin').label('Form Assembly');
model.component('comp1').geom('geom1').feature('fin').set('action', 'assembly');
model.component('comp1').geom('geom1').feature('fin').set('createpairs', false);
model.component('comp1').geom('geom1').run;
model.component('comp1').geom('geom1').run('fin');

%-------------------------------------------------------------
% Detec��o da superf�cie da placa e dos contatos com os eletrodos
x_min = -parametros(1)/2 - 1; x_max = parametros(1)/2 + 1;
y_min = -parametros(2)/2 - 1; y_max = parametros(2)/2 + 1;
z_min = 0; z_max = 0;

caixa_selecao = [x_min x_max; y_min y_max; z_min z_max];
superficie_placa = mphselectbox(model,'geom1',caixa_selecao,'boundary','adjnumber',[1]);
eletrodo1 = mphselectbox(model,'geom1',caixa_selecao,'boundary','adjnumber',[2]);
eletrodo2 = mphselectbox(model,'geom1',caixa_selecao,'boundary','adjnumber',[3]);
contato = [eletrodo1 eletrodo2];

model.component('comp1').pair.create('ap2', 'Identity');
model.component('comp1').pair('ap2').source.set([superficie_placa]);
model.component('comp1').pair('ap2').destination.set([contato]);

% Detec��o das superf�cies superiores dos eletrodos
% Interfaces com a fonte de corrente
z_min = parametros(7); z_max = parametros(7) + 1; % Relacionado com o comprimento dos eletrodos
caixa_selecao = [x_min x_max; y_min y_max; z_min z_max];

contato_fonte = mphselectbox(model,'geom1',caixa_selecao,'boundary');
contato_pos = contato_fonte(1); 
contato_gnd = contato_fonte(2);
%------------------------------

model.component('comp1').material.create('mat1', 'Common');
model.component('comp1').material('mat1').propertyGroup.create('Enu', 'Young''s modulus and Poisson''s ratio');
model.component('comp1').material('mat1').propertyGroup.create('linzRes', 'Linearized resistivity');

model.component('comp1').physics.create('ec', 'ConductiveMedia', 'geom1');
model.component('comp1').physics('ec').create('pelc1', 'PairElectricalContact', 2);
model.component('comp1').physics('ec').create('gnd1', 'Ground', 2);
model.component('comp1').physics('ec').feature('gnd1').selection.set([contato_gnd]);
model.component('comp1').physics('ec').create('term2', 'Terminal', 2);
model.component('comp1').physics('ec').feature('term2').selection.set([contato_pos]);

model.component('comp1').mesh('mesh1').create('ftet1', 'FreeTet');

model.capeopen.label('Thermodynamics Package');

model.component('comp1').view('view1').set('scenelight', false);
model.component('comp1').view('view1').set('showmaterial', true);

model.component('comp1').material('mat1').label('AISI 304.1');
model.component('comp1').material('mat1').set('family', 'copper');
model.component('comp1').material('mat1').propertyGroup('def').set('relpermeability', {'1' '0' '0' '0' '1' '0' '0' '0' '1'});
model.component('comp1').material('mat1').propertyGroup('def').set('electricconductivity', {'1.450E6[S/m]' '0' '0' '0' '1.450E6[S/m]' '0' '0' '0' '1.450E6[S/m]'});
model.component('comp1').material('mat1').propertyGroup('def').set('heatcapacity', '500[J/(kg*K)]');
model.component('comp1').material('mat1').propertyGroup('def').set('relpermittivity', {'1' '0' '0' '0' '1' '0' '0' '0' '1'});
model.component('comp1').material('mat1').propertyGroup('def').set('emissivity', '0.5');
model.component('comp1').material('mat1').propertyGroup('def').set('density', '8000[kg/m^3]');
model.component('comp1').material('mat1').propertyGroup('def').set('thermalconductivity', {'16.2[W/(m*K)]' '0' '0' '0' '16.2[W/(m*K)]' '0' '0' '0' '16.2[W/(m*K)]'});
model.component('comp1').material('mat1').propertyGroup('def').set('youngsmodulus', '126e9[Pa]');
model.component('comp1').material('mat1').propertyGroup('def').set('poissonsratio', '0.34');
model.component('comp1').material('mat1').propertyGroup('Enu').set('youngsmodulus', '126e9[Pa]');
model.component('comp1').material('mat1').propertyGroup('Enu').set('poissonsratio', '0.34');
model.component('comp1').material('mat1').propertyGroup('linzRes').set('rho0', '');
model.component('comp1').material('mat1').propertyGroup('linzRes').set('alpha', '');
model.component('comp1').material('mat1').propertyGroup('linzRes').set('Tref', '');
model.component('comp1').material('mat1').propertyGroup('linzRes').set('rho0', '6.8966e-07[ohm*m]');
model.component('comp1').material('mat1').propertyGroup('linzRes').set('alpha', '9.4*10^-4[1/K]');
model.component('comp1').material('mat1').propertyGroup('linzRes').set('Tref', '293.15[K]');
model.component('comp1').material('mat1').propertyGroup('linzRes').addInput('temperature');

model.common('cminpt').set('minpDefName', 'electricpotential');

model.component('comp1').physics('ec').feature('cucn1').set('materialType', 'solid');
model.component('comp1').physics('ec').feature('cucn1').set('minput_temperature', 'temp');
model.component('comp1').physics('ec').feature('pelc1').set('Tn', 'pressao_contato');
model.component('comp1').physics('ec').feature('pelc1').set('pairs', 'ap2');
model.component('comp1').physics('ec').feature('pelc1').set('materialType', 'from_mat');
model.component('comp1').physics('ec').feature('term2').set('I0', 'corrente');
model.component('comp1').physics('ec').feature('term2').label('Corrente+');

cprintf('blue','Criando malha\n')
model.component('comp1').mesh('mesh1').feature('size').set('hauto', 3);
model.component('comp1').mesh('mesh1').feature('size').set('custom', 'on');
model.component('comp1').mesh('mesh1').feature('size').set('table', 'semi');
% model.component('comp1').mesh('mesh1').feature('size').set('hmax', 0.138);
% model.component('comp1').mesh('mesh1').feature('size').set('hmin', 0.009);
% model.component('comp1').mesh('mesh1').feature('size').set('hnarrow', 0.9);
% model.component('comp1').mesh('mesh1').feature('size').set('hgrad', 1.08);
model.component('comp1').mesh('mesh1').feature('size').set('hmax', 0.69);
model.component('comp1').mesh('mesh1').feature('size').set('hmin', 0.05);
model.component('comp1').mesh('mesh1').feature('size').set('hnarrow', 0.9);
model.component('comp1').mesh('mesh1').feature('size').set('hgrad', 1.1);
model.component('comp1').mesh('mesh1').feature('ftet1').set('optlevel', 'high');
model.component('comp1').mesh('mesh1').feature('ftet1').set('optlarge', true);
model.component('comp1').mesh('mesh1').feature('ftet1').set('optsmall', true);
model.component('comp1').mesh('mesh1').run;

model.component('comp1').physics('ec').feature('cucn1').set('sigma_mat', 'linzRes');

cprintf('blue','Resolvendo\n')
model.study.create('std1');
model.study('std1').create('stat', 'Stationary');

model.sol.create('sol1');
model.sol('sol1').study('std1');
model.sol('sol1').attach('std1');
model.sol('sol1').create('st1', 'StudyStep');
model.sol('sol1').create('v1', 'Variables');
model.sol('sol1').create('s1', 'Stationary');
model.sol('sol1').feature('s1').create('fc1', 'FullyCoupled');
model.sol('sol1').feature('s1').create('i1', 'Iterative');
model.sol('sol1').feature('s1').feature('i1').create('mg1', 'Multigrid');
model.sol('sol1').feature('s1').feature.remove('fcDef');

model.result.dataset.create('dset3', 'Solution');
model.result.dataset.create('cpl2', 'CutPlane');
model.result.dataset.create('cln4', 'CutLine2D');
model.result.dataset.create('cln2', 'CutLine2D');
model.result.dataset('cpl2').set('data', 'dset3');
model.result.dataset.remove('dset1');
model.result.create('pg4', 'PlotGroup1D');
model.result.create('pg14', 'PlotGroup1D');

model.result('pg4').create('lngr1', 'LineGraph');
model.result('pg4').create('lngr3', 'LineGraph');
model.result('pg4').feature('lngr1').set('xdata', 'expr');
model.result('pg4').feature('lngr3').set('xdata', 'expr');

model.result('pg14').create('lngr5', 'LineGraph');
model.result('pg14').create('lngr4', 'LineGraph');
model.result('pg14').feature('lngr5').set('xdata', 'expr');
model.result('pg14').feature('lngr4').set('xdata', 'expr');

model.sol('sol1').attach('std1');
model.sol('sol1').feature('s1').set('stol', 'erro');
model.sol('sol1').feature('s1').feature('i1').set('linsolver', 'cg');
model.sol('sol1').feature('s1').feature('i1').feature('mg1').set('prefun', 'saamg');
model.sol('sol1').runAll;

model.result.dataset('cpl2').label('plano xy na superficie');
model.result.dataset('cpl2').set('quickplane', 'xy');
model.result.dataset('cpl2').set('quickz', '0');
model.result.dataset('cln4').label('linha dos eletrodos ');
model.result.dataset('cln4').set('genpoints', {'-b2/2' 'y_eletrodo'; 'b2/2' 'y_eletrodo'});
model.result.dataset('cln4').set('spacevars', {'cln2x'});
model.result.dataset('cln4').set('normal', {'cln2nx' 'cln2ny'});
model.result.dataset('cln2').label('linha entre eletrodos');
model.result.dataset('cln2').set('genpoints', {'(-b2/2+dist_borda) + afastamento_elet' 'y_eletrodo'; '(b2/2-dist_borda) - afastamento_elet' 'y_eletrodo'});



model.result('pg4').label('Potencial ');
model.result('pg4').set('data', 'cpl2');
model.result('pg4').set('titletype', 'manual');
model.result('pg4').set('title', ['Potencial el' native2unicode(hex2dec({'00' 'e9'}), 'unicode') 'trico ']);
model.result('pg4').set('xlabel', 'x (cm)');
model.result('pg4').set('ylabel', 'Electric potential (mV)');
model.result('pg4').set('xextra', 'range(-15,1,15)');
model.result('pg4').set('legendpos', 'uppermiddle');
model.result('pg4').set('xlabelactive', false);
model.result('pg4').set('ylabelactive', false);
model.result('pg4').feature('lngr1').label('Potencial na linha dos eletrodos');
model.result('pg4').feature('lngr1').set('data', 'cln4');
model.result('pg4').feature('lngr1').set('unit', 'mV');
model.result('pg4').feature('lngr1').set('descractive', true);
model.result('pg4').feature('lngr1').set('descr', ['Potencial el' native2unicode(hex2dec({'00' 'e9'}), 'unicode') 'trico']);
model.result('pg4').feature('lngr1').set('titletype', 'manual');
model.result('pg4').feature('lngr1').set('title', ['Potencial el' native2unicode(hex2dec({'00' 'e9'}), 'unicode') 'trico na linha dos eletrodos']);
model.result('pg4').feature('lngr1').set('xdataexpr', 'x');
model.result('pg4').feature('lngr1').set('xdataunit', 'cm');
model.result('pg4').feature('lngr1').set('xdatadescractive', true);
model.result('pg4').feature('lngr1').set('xdatadescr', 'x');
model.result('pg4').feature('lngr1').set('legend', true);
model.result('pg4').feature('lngr1').set('legendmethod', 'manual');
model.result('pg4').feature('lngr1').set('legends', {'Potencial na linha dos eletrodos'});
model.result('pg4').feature('lngr1').set('resolution', 'extrafine');
model.result('pg4').feature('lngr1').set('smooth', 'internal');
model.result('pg4').feature('lngr1').set('resolution', 'extrafine');
model.result('pg4').feature('lngr3').label('Potencial entre os eletrodos');
model.result('pg4').feature('lngr3').set('data', 'cln2');
model.result('pg4').feature('lngr3').set('plotonsecyaxis', true);
model.result('pg4').feature('lngr3').set('unit', 'mV');
model.result('pg4').feature('lngr3').set('titletype', 'manual');
model.result('pg4').feature('lngr3').set('title', ['Potencial el' native2unicode(hex2dec({'00' 'e9'}), 'unicode') 'trico entre os eletrodos']);
model.result('pg4').feature('lngr3').set('xdataexpr', 'x');
model.result('pg4').feature('lngr3').set('xdataunit', 'cm');
model.result('pg4').feature('lngr3').set('xdatadescractive', true);
model.result('pg4').feature('lngr3').set('xdatadescr', 'x');
model.result('pg4').feature('lngr3').set('legend', true);
model.result('pg4').feature('lngr3').set('legendmethod', 'manual');
model.result('pg4').feature('lngr3').set('legends', {'Potencial entre os eletrodos'});
model.result('pg4').feature('lngr3').set('resolution', 'extrafine');
model.result('pg4').feature('lngr3').set('smooth', 'internal');
model.result('pg4').feature('lngr3').set('resolution', 'extrafine');

model.result('pg14').label('Gradiente do potencial ');
model.result('pg14').set('data', 'cpl2');
model.result('pg14').set('titletype', 'manual');
model.result('pg14').set('title', 'Gradiente do potencial (componente X)');
model.result('pg14').set('xlabel', 'x (cm)');
model.result('pg14').set('ylabel', 'Vx (uV/cm)');
model.result('pg14').set('ylabelactive', true);
model.result('pg14').set('xextra', 'range(-15,1,15)');
model.result('pg14').set('showlegendsmaxmin', true);
model.result('pg14').set('legendpos', 'lowermiddle');
model.result('pg14').set('axisactive', true);
model.result('pg14').set('axisnotation', 'engineering');
model.result('pg14').set('xlabelactive', false);
model.result('pg14').feature('lngr5').label('Gradiente do potencial na linha dos eletrodos');
model.result('pg14').feature('lngr5').set('data', 'cln4');
model.result('pg14').feature('lngr5').set('expr', 'dtang(V,x)');
model.result('pg14').feature('lngr5').set('unit', 'uV/cm');
model.result('pg14').feature('lngr5').set('descractive', true);
model.result('pg14').feature('lngr5').set('descr', ['Potencial el' native2unicode(hex2dec({'00' 'e9'}), 'unicode') 'trico']);
model.result('pg14').feature('lngr5').set('titletype', 'manual');
model.result('pg14').feature('lngr5').set('xdataexpr', 'x');
model.result('pg14').feature('lngr5').set('xdataunit', 'cm');
model.result('pg14').feature('lngr5').set('xdatadescractive', true);
model.result('pg14').feature('lngr5').set('xdatadescr', 'x');
model.result('pg14').feature('lngr5').set('legend', true);
model.result('pg14').feature('lngr5').set('legendmethod', 'manual');
model.result('pg14').feature('lngr5').set('legends', {'Vx na  linha dos eletrodos'});
model.result('pg14').feature('lngr5').set('resolution', 'extrafine');
model.result('pg14').feature('lngr5').set('smooth', 'internal');
model.result('pg14').feature('lngr5').set('resolution', 'extrafine');
model.result('pg14').feature('lngr4').label('Gradiente do potencial entre os eletrodos');
model.result('pg14').feature('lngr4').set('data', 'cln2');
model.result('pg14').feature('lngr4').set('expr', 'dtang(V, x)');
model.result('pg14').feature('lngr4').set('unit', 'uV/cm');
model.result('pg14').feature('lngr4').set('descr', 'dtang(V, x)');
model.result('pg14').feature('lngr4').set('titletype', 'manual');
model.result('pg14').feature('lngr4').set('xdataexpr', 'x');
model.result('pg14').feature('lngr4').set('xdataunit', 'cm');
model.result('pg14').feature('lngr4').set('xdatadescractive', true);
model.result('pg14').feature('lngr4').set('xdatadescr', 'x');
model.result('pg14').feature('lngr4').set('legend', true);
model.result('pg14').feature('lngr4').set('legendmethod', 'manual');
model.result('pg14').feature('lngr4').set('legends', {'Vx entre os eletrodos'});
model.result('pg14').feature('lngr4').set('resolution', 'extrafine');
model.result('pg14').feature('lngr4').set('smooth', 'internal');
model.result('pg14').feature('lngr4').set('resolution', 'extrafine');

% C�lculo do m�dulo do gradiente
if salva == 1
    model.result.export.create('plot15', 'Plot');
    model.result.create('pg15', 'PlotGroup2D');
    model.result('pg15').create('surf1', 'Surface');
    model.result('pg15').label(['M' native2unicode(hex2dec({'00' 'f3'}), 'unicode') 'dulo do gradiente']);
    model.result('pg15').set('xlabel', 'x');
    model.result('pg15').set('xlabelactive', true);
    model.result('pg15').set('ylabel', 'y');
    model.result('pg15').set('ylabelactive', true);
    model.result('pg15').set('showlegendsmaxmin', true);
    model.result('pg15').set('showlegendsunit', true);
    model.result('pg15').set('legendactive', true);
    model.result('pg15').set('legendtrailingzeros', true);
    model.result('pg15').set('legendprecision', 4);
    model.result('pg15').feature('surf1').label('||gradV||');
    model.result('pg15').feature('surf1').set('expr', 'sqrt((dtang(V,x))^2 + (dtang(V,y))^2)');
    model.result('pg15').feature('surf1').set('unit', 'uV/cm');
    model.result('pg15').feature('surf1').set('descr', 'sqrt((dtang(V,x))^2 + (dtang(V,y))^2)');
    model.result('pg15').feature('surf1').set('titletype', 'manual');
%     model.result('pg15').feature('surf1').set('title', 'Componente x do gradiente - placa com defeito');
    model.result('pg15').feature('surf1').set('rangecoloractive', true);
    model.result('pg15').feature('surf1').set('rangecolormin', 0);
    model.result('pg15').feature('surf1').set('rangecolormax', 80);
    model.result('pg15').feature('surf1').set('rangedataactive', true);
    model.result('pg15').feature('surf1').set('rangedatamin', 0);
    model.result('pg15').feature('surf1').set('rangedatamax', 80);
    model.result('pg15').feature('surf1').set('resolution', 'extrafine');

    cprintf('blue','Salvando M�dulo do Gradiente\n')
    model.result.export('plot15').label('M�dulo Gradiente na superficie');
    model.result.export('plot15').set('plotgroup', 'pg15');

    model.result.export('plot15').set('filename', strcat(endereco,'grad_modulo_superficie_placa_completa.txt'));
    model.result.export('plot15').set('header', true);
    model.result.export('plot15').set('sort', true);
    model.result.export('plot15').run;

    model.result('pg15').run;
    model.result('pg15').feature('surf1').set('title', ['M' native2unicode(hex2dec({'00' 'f3'}), 'unicode') 'dulo do gradiente - placa com defeito']);
end

if salva == 0
    model.result.create('pg13', 'PlotGroup2D');
    model.result('pg13').create('surf1', 'Surface');
    model.result('pg13').create('surf2', 'Surface');
    model.result('pg13').label('Componentes do gradiente');
    model.result('pg13').set('xlabel', 'x');
    model.result('pg13').set('xlabelactive', true);
    model.result('pg13').set('ylabel', 'y');
    model.result('pg13').set('ylabelactive', true);
    model.result('pg13').set('showlegendsmaxmin', true);
    model.result('pg13').set('showlegendsunit', true);
    model.result('pg13').set('legendactive', true);
    model.result('pg13').set('legendtrailingzeros', true);
    model.result('pg13').set('legendprecision', 4);
    model.result('pg13').feature('surf1').label('Comp x');
    model.result('pg13').feature('surf1').set('expr', 'dtang(V,x)');
    model.result('pg13').feature('surf1').set('unit', 'uV/cm');
    model.result('pg13').feature('surf1').set('descr', 'dtang(V,x)');
    model.result('pg13').feature('surf1').set('titletype', 'manual');
    model.result('pg13').feature('surf1').set('title', 'Componente x do gradiente - placa com defeito');
    model.result('pg13').feature('surf1').set('resolution', 'extrafine');
    model.result('pg13').feature('surf2').label('Comp y');
    model.result('pg13').feature('surf2').set('expr', 'dtang(V,y)');
    model.result('pg13').feature('surf2').set('unit', 'uV/cm');
    model.result('pg13').feature('surf2').set('descr', 'dtang(V,y)');
    model.result('pg13').feature('surf2').set('titletype', 'manual');
    model.result('pg13').feature('surf2').set('title', 'Componente y do gradiente - placa com defeito');
    model.result('pg13').feature('surf2').set('resolution', 'extrafine');
    model.result.export.create('plot9', 'Plot');
    model.result.export.create('plot10', 'Plot');

    cprintf('blue','Salvando Gradiente na superficie - comp x\n')
    model.result.export('plot9').label('Gradiente na superficie - comp x');
    model.result.export('plot9').set('filename', strcat(endereco,'grad_comp_x_superficie_placa_completa.txt'));
    model.result.export('plot9').set('header', false);
    model.result.export('plot9').set('sort', true);
    model.result.export('plot9').run;

    cprintf('blue','Salvando Gradiente na superficie - comp y\n')
    model.result.export('plot10').label('Gradiente na superficie - comp y');
    model.result.export('plot10').set('plot', 'surf2');
    model.result.export('plot10').set('filename', strcat(endereco,'grad_comp_y_superficie_placa_completa.txt'));
    model.result.export('plot10').set('header', true);
    model.result.export('plot10').set('sort', true);
    model.result.export('plot10').run;
end
% cprintf('blue','Salvando Potencial na linha dos eletrodos\n')
% model.result.export.create('plot1', 'Plot');
% model.result.export('plot1').label('Potencial na linha dos eletrodos');
% model.result.export('plot1').set('plotgroup', 'pg4');
% model.result.export('plot1').set('plot', 'lngr1');
% model.result.export('plot1').set('filename', strcat(endereco,'potencial_linha_dos_eletrodos.txt'));
% model.result.export('plot1').set('header', false);
% model.result.export('plot1').set('sort', true);
% model.result.export('plot1').run;

% cprintf('blue','Salvando Potencial entre os eletrodos\n')
% model.result.export.create('plot14', 'Plot');
% model.result.export('plot14').label('Potencial entre os eletrodos');
% model.result.export('plot14').set('plotgroup', 'pg4');
% model.result.export('plot14').set('plot', 'lngr3');
% model.result.export('plot14').set('filename', strcat(endereco,'potencial_entre_eletrodos.txt'));
% model.result.export('plot14').set('header', false);
% model.result.export('plot14').set('sort', true);
% model.result.export('plot14').run;

% cprintf('blue','Salvando Dens corrente na linha dos eletrodos - comp x\n')  
% model.result.create('pg9', 'PlotGroup1D');
% model.result('pg9').create('lngr2', 'LineGraph');
% model.result('pg9').feature('lngr2').set('xdata', 'expr');
% model.result('pg9').label('Densidade de corrente');
% model.result('pg9').set('data', 'cpl2');
% model.result('pg9').set('titletype', 'manual');
% model.result('pg9').set('title', ['Potencial el' native2unicode(hex2dec({'00' 'e9'}), 'unicode') 'trico e densidade de corrente (componente X) entre os eletrodos']);
% model.result('pg9').set('xlabel', 'x (cm)');
% model.result('pg9').set('ylabel', ['Jx (A/m' native2unicode(hex2dec({'00' 'b2'}), 'unicode') ')']);
% model.result('pg9').set('ylabelactive', true);
% model.result('pg9').set('ylog', true);
% model.result('pg9').set('xextra', 'range(-11,1,11)');
% model.result('pg9').set('showlegendsmaxmin', true);
% model.result('pg9').set('legendpos', 'uppermiddle');
% model.result('pg9').set('axisactive', true);
% model.result('pg9').set('axisnotation', 'engineering');
% model.result('pg9').set('xlabelactive', false);
% model.result('pg9').feature('lngr2').label('Dens corrente na linha dos eletrodos - comp x');
% model.result('pg9').feature('lngr2').set('data', 'cln4');
% model.result('pg9').feature('lngr2').set('expr', 'ec.Jx');
% model.result('pg9').feature('lngr2').set('unit', 'A/m^2');
% model.result('pg9').feature('lngr2').set('descr', 'Current density, x component');
% model.result('pg9').feature('lngr2').set('titletype', 'manual');
% model.result('pg9').feature('lngr2').set('xdataexpr', 'x');
% model.result('pg9').feature('lngr2').set('xdataunit', 'cm');
% model.result('pg9').feature('lngr2').set('xdatadescractive', true);
% model.result('pg9').feature('lngr2').set('xdatadescr', 'x');
% model.result('pg9').feature('lngr2').set('legend', true);
% model.result('pg9').feature('lngr2').set('legendmethod', 'manual');
% model.result('pg9').feature('lngr2').set('legends', {'Dens corrente na linha dos eletrodos'});
% model.result('pg9').feature('lngr2').set('resolution', 'extrafine');
% model.result('pg9').feature('lngr2').set('smooth', 'internal');
% model.result('pg9').feature('lngr2').set('resolution', 'extrafine');

% model.result.export.create('plot16', 'Plot');
% model.result.export('plot16').label('Dens corrente na linha dos eletrodos - comp x');
% model.result.export('plot16').set('plotgroup', 'pg9');
% model.result.export('plot16').set('plot', 'lngr2');
% model.result.export('plot16').set('filename', strcat(endereco,'Jx_na_linha_dos_eletrodos.txt'));
% model.result.export('plot16').set('header', false);
% model.result.export('plot16').set('sort', true);
% model.result.export('plot16').run;

% cprintf('blue','Salvando Dens corrente entre os eletrodos\n')
% model.result('pg9').create('lngr3', 'LineGraph');
% model.result('pg9').feature('lngr3').set('xdata', 'expr');
% model.result('pg9').feature('lngr3').label('Dens corrente entre os eletrodos - comp x');
% model.result('pg9').feature('lngr3').set('data', 'cln2');
% model.result('pg9').feature('lngr3').set('expr', 'ec.Jx');
% model.result('pg9').feature('lngr3').set('unit', 'A/m^2');
% model.result('pg9').feature('lngr3').set('descr', 'Current density, x component');
% model.result('pg9').feature('lngr3').set('titletype', 'manual');
% model.result('pg9').feature('lngr3').set('xdataexpr', 'x');
% model.result('pg9').feature('lngr3').set('xdataunit', 'cm');
% model.result('pg9').feature('lngr3').set('xdatadescractive', true);
% model.result('pg9').feature('lngr3').set('xdatadescr', 'x');
% model.result('pg9').feature('lngr3').set('legend', true);
% model.result('pg9').feature('lngr3').set('legendmethod', 'manual');
% model.result('pg9').feature('lngr3').set('legends', {'Dens corrente entre os eletrodos'});
% model.result('pg9').feature('lngr3').set('resolution', 'extrafine');
% model.result('pg9').feature('lngr3').set('smooth', 'internal');
% model.result('pg9').feature('lngr3').set('resolution', 'extrafine');
% model.result.export.create('plot5', 'Plot');
% model.result.export('plot5').label('Dens corrente entre os eletrodos');
% model.result.export('plot5').set('plotgroup', 'pg9');
% model.result.export('plot5').set('plot', 'lngr3');
% model.result.export('plot5').set('filename', strcat(endereco,'Jx_entre_eletrodos.txt'));
% model.result.export('plot5').set('header', false);
% model.result.export('plot5').set('sort', true);
% model.result.export('plot5').run;

% cprintf('blue','Salvando Gradiente do potencial na linha dos eletrodos - comp x\n')
% model.result.export.create('plot13', 'Plot');
% model.result.export('plot13').label('Gradiente do potencial na linha dos eletrodos - comp x');
% model.result.export('plot13').set('plotgroup', 'pg14');
% model.result.export('plot13').set('plot', 'lngr5');
% model.result.export('plot13').set('filename', strcat(endereco,'comp_x_gradiente_potencial_linha_dos_eletrodos.txt'));
% model.result.export('plot13').set('header', false);
% model.result.export('plot13').set('sort', true);
% model.result.export('plot13').run;

% cprintf('blue','Salvando Gradiente do potencial entre os eletrodos - comp x\n')
% model.result.export.create('plot15', 'Plot');
% model.result.export('plot15').label('Gradiente do potencial entre os eletrodos');
% model.result.export('plot15').set('plotgroup', 'pg14');
% model.result.export('plot15').set('plot', 'lngr4');
% model.result.export('plot15').set('filename', strcat(endereco,'comp_x_gradiente_potencial_entre_os_eletrodos.txt'));
% model.result.export('plot15').set('header', false);
% model.result.export('plot15').set('sort', true);
% model.result.export('plot15').run;

out = model;
