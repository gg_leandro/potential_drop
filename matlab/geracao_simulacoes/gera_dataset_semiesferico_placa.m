% Realiza a simula��o da placa quadrada com defeitos semiesf�ricos de forma automatizada
% Permite definir intervalo de coordenadas (x e y) que cont�m os defeitos
% Permite salvar os modelos gerados, os arquivos txt das simula��es
% Monitora o tempo de simula��o e salva-o em txt
    
clc; clear all; close all

% Pela ASTM46, em termos de �rea, o maior defeito (B5) com 24.5mm^2, que �
% equivalente ao di�metro de 5.53mm

endereco_base = regexp(pwd,'matlab','split');
endereco_base = string(endereco_base(1));
addpath(genpath(endereco_base));

% Endere�o da raiz onde ser�o salvos/lidos os resultados
endereco_auxiliar = strcat(endereco_base,'simulacoes\com_defeito\');

if ~exist(endereco_auxiliar, 'dir') % Cria��o da pasta onde ser�o salvos os dados das simula��es
   mkdir(endereco_auxiliar); 
end


num_exp = 10; % N�mero total de simula��es
num_exp_bloco = 2; % N�mero de simula��es por rodada
num_esf_por_exp = 5; % N�mero de defeitos
area_min = 1.8; area_max = 7.99; %Em mm^2
min_coord_x = -80; max_coord_x = 80; % Intervalo de coordenadas dos defeitos, em mm
min_coord_y = -80; max_coord_y = 80; % Intervalo de coordenadas dos defeitos, em mm

parametros = ones(1,14);
parametros(1) = 30; % comprimento da pe�a (cm)
parametros(2) = 30; % largura da pe�a (cm)
parametros(3) = 10; % espessura da pe�a (mm)
parametros(4) = 3; % dist�ncia do eletrodo � borda, coordenada em x (cm) % Sem uso, por�m mantida
parametros(5) = 0; % altura do eletrodo, coordenada em y (cm)
parametros(6) = .6; % di�metro do contato (mm)
parametros(7) = .2; % comprimento do eletrodo (mm)
parametros(8) = 30; % temperatura (�C)
parametros(9) = 10; % corrente (A)
parametros(10) = 1E-5; % erro
parametros(11) = 35; % press�o de contato (MPa)
parametros(12) = 1; % afastamento da zona de perturba��o, na vizinhan�a dos eletrodos (cm)
parametros(13) = -12; % x_eletrodo_pos (cm)
parametros(14) = 12; % x_eletrodo_neg  (cm)

if ~exist(endereco_base, 'dir') % Cria��o da pasta onde ser�o salvos os dados das simula��es
   mkdir(endereco_base); 
end

% Se j� h� dados sobre �reas e coordenadas salvos, eles s�o reutilizados; 
% caso contr�rio ser�o calculados e salvos
try
    area = table2array(readtable(strcat(endereco_auxiliar,'_area',num2str(area_min),'-',num2str(area_max),'_',num2str(num_esf_por_exp),'_',num2str(num_exp),'.txt')));
    x = table2array(readtable(strcat(endereco_auxiliar,'_x',num2str(min_coord_x),'-',num2str(max_coord_x),'_',num2str(num_esf_por_exp),'_',num2str(num_exp),'.txt')));
    y = table2array(readtable(strcat(endereco_auxiliar,'_y',num2str(min_coord_y),'-',num2str(max_coord_y),'_',num2str(num_esf_por_exp),'_',num2str(num_exp),'.txt')));
catch
    rng('shuffle');
    area = area_min + (area_max - area_min)*rand([num_esf_por_exp,num_exp]);
    area = round(1000*area,1)/1000;
    media_area = (area_min + area_max)/2;


    x = min_coord_x + (max_coord_x - min_coord_x)*rand([num_esf_por_exp,num_exp]);
    x = round(1000*x,1)/1000;
    y = min_coord_y + (max_coord_y - min_coord_y)*rand([num_esf_por_exp,num_exp]);
    y = round(1000*y,1)/1000;

    salva_area = zeros([num_esf_por_exp,num_exp]);
    salva_area(:,:) = area;
    salva_x = zeros([num_esf_por_exp,num_exp]);
    salva_x(:,:) = x;
    salva_y = zeros([num_esf_por_exp,num_exp]);
    salva_y(:,:) = y;
    
    col_names = compose(strcat('simulacao', '_', '%d'), 1:num_exp)
    T = array2table(salva_area, 'VariableNames', col_names);
    writetable(T, strcat(endereco_auxiliar,'_area',num2str(area_min),'-',num2str(area_max),'_',num2str(num_esf_por_exp),'_',num2str(num_exp),'.txt'),'QuoteStrings',true);
    T = array2table(salva_x, 'VariableNames', col_names);
    writetable(T, strcat(endereco_auxiliar,'_x',num2str(min_coord_x),'-',num2str(max_coord_x),'_',num2str(num_esf_por_exp),'_',num2str(num_exp),'.txt'),'QuoteStrings',true);
    T = array2table(salva_y, 'VariableNames', col_names);
    writetable(T, strcat(endereco_auxiliar,'_y',num2str(min_coord_y),'-',num2str(max_coord_y),'_',num2str(num_esf_por_exp),'_',num2str(num_exp),'.txt'),'QuoteStrings',true);
end
z = -parametros(3)*ones(1,num_esf_por_exp); 


figure
histogram(area,'NumBins',50);
title('Distribui��o das �reas dos defeitos - normal')
xlabel('�reas (mm�)')
ylabel('Frequ�ncia')
grid on

% C�lculo raios
raio = sqrt(area/pi);
raio = round(1000*raio,1)/1000;

tempo = ones(num_exp,2);
tempo(1:num_exp,1) = 1:num_exp;
tempo_lido = [];
try % Recupera��o da �tima simula��o realizada
    tempo_lido = table2array(readtable(strcat(endereco_auxiliar,'_tempos.txt')));
    ult_sim = find(tempo_lido(:,2) ~= 1,1,'last'); % Recupera o n�mero da �ltima  simula�ao realizada
    tempo(1:ult_sim,2) = tempo_lido(:,2);
catch
    if(isempty(tempo_lido))
      ult_sim = 0;
    end    
end


for i = ult_sim+1:ult_sim + num_exp_bloco
    cprintf('Comments','Experimento %i\n',i);
    tic;
    endereco = strcat(endereco_auxiliar,'\','simulacao',num2str(i),'\'); 
            
    if ~exist(endereco, 'dir') % Cria��o da pasta onde ser�o salvos os dados das simula��es
       mkdir(endereco); 
    end

    defeito = ones(num_esf_por_exp,5); % matriz que recebe os par�metros dos defeitos simulados
    defeito(:,1) = area(:,i)';
    defeito(:,2) = raio(:,i)';
    defeito(:,3) = x(:,i)';
    defeito(:,4) = y(:,i)';
    defeito(:,5) = z;

    % Exporta��o do modelo criado
    modelo = gera_modelo_quadrado(parametros,endereco,defeito,1);
    mphsave(modelo,strcat(endereco,'\geometria_quadrada_com_esferas.mph'))

    tempo(i,2) = toc;
    
    if(~isempty(tempo_lido))
        tempo(1:ult_sim,2) = tempo_lido(1:ult_sim,2);
    end
    cprintf('*Strings','Tempo gasto: %.3f segundos \n\n',tempo(i,2));
     
    % Armazenamento dos arquivos com os par�metros dos defeitos e dos
    % tempos de simula��o
    T = array2table(tempo,'VariableNames',{'experimento','tempo'});
    writetable(T, strcat(endereco_auxiliar,'_tempos.txt'),'QuoteStrings',true);
    T = array2table(defeito,'VariableNames',{'area','raio','x','y','z'});
    writetable(T, strcat(endereco,'param_defeitos.txt'),'QuoteStrings',true);
    
end

% desliga_pc();
