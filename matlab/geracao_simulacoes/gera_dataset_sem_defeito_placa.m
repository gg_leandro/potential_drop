% Realiza a simula��o da placa quadrada sem defeitos de forma automatizada
% Permite salvar os modelos gerados e os arquivos txt das simula��es
% Monitora o tempo de simula��o e salva-o em txt
    
clc;clear all;close all


root_folder = regexp(pwd,'matlab','split');
root_folder = string(root_folder(1));
addpath(genpath(root_folder));
% Endere�o da raiz onde ser�o salvos/lidos os resultados
endereco_base = strcat(root_folder,'\simulacoes\sem_defeito');
num_exp = 2; % N�mero de simula��es

parametros = ones(1,14);
parametros(1) = 30; % largura da pe�a (cm)
parametros(2) = 30; % altura da pe�a (cm)
parametros(3) = 10; % espessura da pe�a (mm)
parametros(4) = 3; % dist�ncia do eletrodo � borda, coordenada em x (cm)
parametros(5) = 0; % altura do eletrodo, coordenada em y (cm)
parametros(6) = .6; % di�metro do contato (mm)
parametros(7) = .2; % comprimento do eletrodo (mm)
parametros(8) = 30; % temperatura (�C)
parametros(9) = 10; % corrente (A)
parametros(10) = 1E-5; % erro
parametros(11) = 35; % press�o de contato (MPa)
parametros(12) = 1; % afastamento da zona de perturba��o, na vizinhan�a dos eletrodos (cm)
parametros(13) = -12; % x_eletrodo_pos
parametros(14) = 12; % x_eletrodo_neg  


tempo = ones(num_exp,2);
tempo(1:num_exp,1) = 1:num_exp;
for i = 1:num_exp
    tic;
    endereco = strcat(endereco_base,'\','simulacao',num2str(i),'\'); 
    endereco_auxiliar = strcat(endereco_base,'\simulacao');
    cprintf('Comments','Experimento %i\n',i)
    if ~exist(endereco, 'dir') % Cria��o da pasta onde ser�o salvos os dados das simula��es
       mkdir(endereco); 
    end
        
    modelo = gera_modelo_quadrado(parametros,endereco,[],1);

    mphsave(modelo,strcat(endereco,'\geometria_quadrada_sem_defeito.mph'))

    tempo(i,2) = toc;
    cprintf('*Strings','Tempo gasto: %.3f segundos \n',tempo(i,2));
    % Armazenamento do do arquivo com os tempos gastos
    T = array2table(tempo,'VariableNames',{'experimento','tempo'});
    writetable(T, strcat(endereco_auxiliar,'_tempos.txt'),'QuoteStrings',true);

end


