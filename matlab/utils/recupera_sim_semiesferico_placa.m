% Constr�i modelos a partir das especifica��es dos defeitos em arquivo .txt no formato: 
%           diametro,x,y,z
%           3.5,-33.43,49.9,-10
%           3.58,-16.79,37.03,-10
%           3.72,3.54,-24.95,-10
%           3.58,-20.82,16.81,-10
%           3.6,-0.53,6.93,-10
% Neste caso seria criado um modelo com 5 defeitos com as caracter�sticas dadas

clc;clear all;close all

% Endere�o da raiz onde ser�o lidos os defeitos
endereco_base = '';
num_exp = 1;
min_coord = -50; max_coord = 50; % Intervalo de coordenadas dos defeitos, em mm

parametros = ones(1,14);
parametros(1) = 30; % comprimento da pe�a (cm)
parametros(2) = 30; % largura da pe�a (cm)
parametros(3) = 10; % espessura da pe�a (mm)
parametros(4) = 3; % dist�ncia do eletrodo � borda, coordenada em x (cm) % Sem uso
parametros(5) = 0; % altura do eletrodo, coordenada em y (cm)
parametros(6) = .6; % di�metro do contato (mm)
parametros(7) = .2; % comprimento do eletrodo (mm)
parametros(8) = 30; % temperatura (�C)
parametros(9) = 10; % corrente (A)
parametros(10) = 1E-5; % erro
parametros(11) = 35; % press�o de contato (MPa)
parametros(12) = 1; % afastamento da zona de perturba��o, na vizinhan�a dos eletrodos (cm)
parametros(13) = -12; % x_eletrodo_pos
parametros(14) = 12; % x_eletrodo_neg  

                     
for i = 1:num_exp
    cprintf('Comments','Experimento %i\n',i);
    endereco = strcat(endereco_base,'\','simulacao',num2str(i),'\');
    
    % matriz que recebe os par�metros dos defeitos simulados
    defeito = table2array(readtable(strcat(endereco,'param_defeitos.txt')));

    % Exporta��o do modelo criado
    modelo = gera_modelo_quadrado(parametros,endereco,defeito,1);
    mphsave(modelo,strcat(endereco,'\sem_defeito.mph'))  
end

