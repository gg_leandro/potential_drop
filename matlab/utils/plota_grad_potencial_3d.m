function plota_grad_potencial_3d(arquivo_dados,lim_interp,tit_graf,lim_escala)

min_interp = lim_interp(1)/10;
max_interp = lim_interp(2)/10;
x1 = min_interp:.25:max_interp; % Novo espa�o
y1 = min_interp:.25:max_interp;


% Cria��o da imagem el�trica
coord_x = arquivo_dados(:,1);
coord_y = arquivo_dados(:,2);
valor_func = arquivo_dados(:,3);
[xx,yy] = meshgrid(x1,y1);
interpolado = griddata(coord_x,coord_y,valor_func,xx,yy);


fig=figure;
surf(xx,yy,interpolado); % microvolts/div
view([-10 9])
fig.CurrentAxes.XLim=([min_interp max_interp]);
fig.CurrentAxes.YLim=([min_interp max_interp]);

fig.CurrentAxes.ZLim=(lim_escala);
colormap parula;
c = colorbar;
c.Label.String='||\nabla V|| (\muV/cm)';
caxis(lim_escala); % Mudar range de cores
xlabel('{\it x} (cm)');
ylabel('{\it y} (cm)');
set(get(gca,'ylabel'),'rotation',-45)
zlabel('Gradiente do potencial, ||\nabla V|| (\muV/cm)');
set(gca,'ZTick',lim_escala(1):1.0:lim_escala(length(lim_escala)));

% zlabel('Potential gradient, ||\nabla V|| (\muV/cm)');
title(tit_graf,'FontSize',12);
[lin,col] = size(interpolado);
valor = interpolado(round(lin/2),round(col/2));
txt = strcat('\textbf{',num2str(valor),'}','$\mu$V/cm');
text('Interpreter','latex','String',txt,'Position',[-1 0 valor+.7],'FontSize',13,'Color','red')%.95 .7
hold on
% p = plot3(0,0,valor+.75, '*','Color','red');
% p.LineWidth = 1.0;
tt = text('Interpreter','latex','String','\textbf{$\downarrow$}','Position',[-.105 0 valor+.3],'FontSize',25,'Color','red');
tt.FontWeight = 'bold';


end