function plota_curvas_nivel_def(arquivo_dados,lim_interp,num_curvas,tit_graf,lim_escala,defeito,conta_pits,eixoy,color_bar)

    min_interp = lim_interp(1)/10;
    max_interp = lim_interp(2)/10;
    x1 = min_interp:.1:max_interp; % Novo espa�o
    y1 = min_interp:.1:max_interp;

    
    % Cria��o da imagem el�trica
    coord_x = arquivo_dados(:,1);
    coord_y = arquivo_dados(:,2);
    valor_func = arquivo_dados(:,3);
    [xx,yy] = meshgrid(x1,y1);
    interpolado = griddata(coord_x,coord_y,valor_func,xx,yy);

    % Visualiza��o dos dados limpos
%     figure
    contour(x1,y1,interpolado,num_curvas);
    if ~isempty(defeito)
        defeito = defeito./10; % Convers�o para cent�metro
        teta = 0:0.01:2*pi;
        hold on
        [lin,~] = size(defeito);
        for i = 1:lin
            x = (defeito(i,1)/2) * cos(teta) + defeito(i,2);
            y = (defeito(i,1)/2) * sin(teta) + defeito(i,3);
            pt=plot(x, y,'k');
            pt.LineWidth = 1.0;
%             axis square;
%             %grid on;
%             axis equal;
            if conta_pits ~=0
                txt = strcat('\textbf{Alv\''eolo} ',num2str(i));
            else
                txt = '\textbf{$\leftarrow$ Pit}';

            end
%             text('Interpreter','latex','String',txt,'Position',[defeito(i,2)-.5 defeito(i,3)-.55],'FontSize',12)
            posicao_x = defeito(i,2)+.35;
            if posicao_x +1 > max_interp
               posicao_x = posicao_x -2.5; 
            end
    
%             text('Interpreter','latex','String',txt,'Position',[posicao_x defeito(i,3)-.25],'FontSize',12,'Color','k')

        end
%         text('Interpreter','latex','String','\textbf{$\leftarrow$ Pit}','Position',[defeito(1,1)/2 0],'FontSize',12)
        hold off
    end
    axis square;
    axis equal;
    title(tit_graf,'FontSize',9);
    colormap parula;
    caxis(lim_escala); % Mudar range de cores
    if color_bar ~= 0
        c = colorbar;
        c.Label.String = '||\nabla V|| (\muV/cm)';
        c.Label.FontSize = 8;
    end
    xlabel('{\it x} (cm)');
    if eixoy ~= 0
        ylabel('{\it y} (cm)');
        set(gca,'YTick',min_interp:2.0:max_interp);
    else
        set(gca,'YTickLabel',[]);
    end
    
    set(gca,'XTick',min_interp:2.0:max_interp);
    
    
    shading interp
end
