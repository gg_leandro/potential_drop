% 	Primeira fun��o criada para gerar curvas de n�vel do gradiente de potencial na placa quadrada com defeito quadrado
% 	N�o h� limpeza de outliers
% 		Os par�metros de entrada s�o:
% 		x - Vetor com as coordenadas x
% 		y - Vetor com as coordenadas y
% 		f - Vetor com os valores da fun��o que ser� plotada
% 		tit - T�tulo do gr�fico
% 		num_cont - N�mero de curvas de n�vel
% 		lim - Limite da escala de cores
% 		defeito - Valor bin�rio para decidir se h� plotagem do defeito ou n�o
         
function plota_contorno_basico(x,y,f,num_cont,tit,lim,defeito)
    figure
    contour(x,y,f,num_cont);shading interp
    title(tit);
    colormap parula;
    axis square;
    %grid on;
    axis equal;
    caxis(lim); % Mudar range de cores
    c = colorbar;
    c.Label.String = '\muV/cm';
    xlabel('Eixo X (cm)');
    ylabel('Eixo Y (cm)');
%     shading interp
    if defeito == 1 % Plota defeito
        hold on
        a = plot([-2 2 2 -2 -2],[-2 -2 2 2 -2],'--k');
        legend(a,'Regi�o de interesse')
        hold off
    end
end

