close all; clc; clear all


inicio = 1;
num_exp = 1000;

diametro = zeros(5,num_exp - inicio + 1);
endereco_base = 'H:\simulacoes_dissertacao\big_data_b3-b5\';   


for i = inicio:num_exp
    defeito = table2array(readtable(strcat(endereco_base,'simulacao',num2str(i),'\param_defeitos.txt')));
    diametro(:,i-inicio+1) = defeito(:,1);
end
figure
histogram(diametro,'NumBins',50);
title('Distribui��o dos di�metros dos defeitos')
xlabel('Di�metros (mm)')
ylabel('Frequ�ncia')
grid on