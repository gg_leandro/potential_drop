# -*- coding: utf-8 -*-
#"""
#Created on Wed Feb 12 10:34:26 2020
#
#@author: George Leandro
#"""

"""Este código realiza o processamento dos dados simulados, a partir da referência (placa sem defeito)
é gerado o módulo do gradiente de acordo com a malha simulada,
depois é feita a interpolação num grid entre eixo_x e eixo_y. 
Por fim são geradas as imagens elétricas e os respectivos labels."""

print(__doc__)

import numpy as np
from copy import copy
import cv2
import sys
import matplotlib.pyplot as plt
from scipy import ndimage
import pandas as pd
from datetime import date
import utils as utils
from skimage.feature import peak_local_max
from skimage.segmentation import watershed
import imutils 
import os
from scipy import stats
from PIL import Image
import sys

cur_dir = os.path.dirname(os.path.abspath(__file__))

    
"""->->->->->->->->->-> Configuração inicial <-<-<-<-<-<-<-<-<-<-"""
dir_data_final = "data"                       # Diretório onde será salvo o dataset

dir_data_final = os.path.join(cur_dir,"exemplo", dir_data_final)
endereco_sem_defeito = os.path.join(cur_dir,"exemplo","sem_defeito", "grad_modulo_medio.txt")
dir_dataset = os.path.join(cur_dir, "exemplo", "grad_modulo_superficie_comsol")
endereco_defeitos = os.path.join(cur_dir, "exemplo", "param_defeitos")
endereco_grad_interp_base = os.path.join(cur_dir, "exemplo", "grad_modulo_superficie_interpolado")

# Intervalo onde será feita a interpolação do gradiente de potencial
lim_min_x = 0
lim_max_x = 80
lim_min_y = -80  
lim_max_y = 0

# Exibição das regiões segmentadas (se ativa prejudica o a velocidade de processamento, mas pode ser útil em pequenos debugs)
exibicao_seg = 0 
    
"""->->->->->->->->->-> Fim configuração inicial <-<-<-<-<-<-<-<-<-<-"""

simulacoes = os.listdir(dir_dataset)
simulacoes = [simulacoes[i] for i in range(len(simulacoes)) if simulacoes[i].endswith(".txt")]
def main():
    
    data = str(date.today())
    nome_csv = os.path.join(dir_data_final, "dataset_" + data + ".csv")
    porcentagem = .1, .2, .3, .4, .5, .6, .7, .8, .9
    tam_caixa = 32
    limiar_menor_dist = 4 
    tam_padding = 20
    area_classe = []; area_roi = []
    maximo_astm_referencia = 1.0229691261256335
       
 

    eixo_x = np.arange(lim_min_x,lim_max_x+1,1)
    eixo_y = np.arange(lim_min_y,lim_max_y+1,1)
    eixo_x_cm = eixo_x/10 # Conversão para cm, pois o COMSOL está salvando o gradiente em cm
    eixo_y_cm = eixo_y/10
    
    grad_modulo_sem_defeito = utils.gera_ref_sem_defeito(endereco_sem_defeito,eixo_x_cm,eixo_y_cm)
    
    num_ROI = 0
    debug = []
    lista_treino = []
    num_tot_reg_segmentadas = 0

    for sim in simulacoes:
        
        processando = os.path.splitext(sim)[0]
        print("Processando {}".format(processando))
        # Verficação da existência dos arquivos fundmanentais: módulo do gradiente interpolado e também o gradiente bruto
        # Geração/obtenção do módulo do gradiente "bruto" #
        endereco_grad_bruto = os.path.join(dir_dataset, sim)#grad_modulo.txt"
        endereco_grad_interp = os.path.join(endereco_grad_interp_base, processando + "_"+str(lim_min_x) \
                                +"_"+str(lim_max_x)+"_"+str(lim_min_y)+"_"+str(lim_max_y)+".txt")
                                
        try: # Aproveita  os dados já salvos                 
            grad_interpolado = np.array(pd.read_csv(endereco_grad_interp,header=None))
        except FileNotFoundError: # Se não houver valores salvos
            try: #    verifica se há os módulos dos gradientes salvos 
                grad_modulo = utils.txt_matlab_comsol_2py(endereco_grad_bruto)
                grad_interpolado = utils.interpola_grad(grad_modulo,eixo_x_cm,eixo_y_cm)
                grad_interpolado = np.divide(grad_interpolado,grad_modulo_sem_defeito) # divisão ponto a ponto                         
                np.savetxt(endereco_grad_interp, grad_interpolado, delimiter=",", comments='',fmt='%.10f')
            except Exception:                
                sys.exit("\033[1;31;47m Número de simulações ou endereço inválido!")                                   
                        
        # Normalização com base no máximo defeito da ASTM
        assinatura_ref = copy(grad_interpolado)
        grad_interpolado = 255*(1-((grad_interpolado-1)/(maximo_astm_referencia - 1))) # Normalização
        remove_nan = np.where(np.isnan(grad_interpolado)) # Nas bordas da placa podem ocorrer NaN's por causa da interpolação
        grad_interpolado[remove_nan] = 255
        t = np.where(grad_interpolado > 255)
        grad_interpolado[t] = 255
        t = np.where(grad_interpolado < 0)
        grad_interpolado[t] = 1e-5 # Se for igual a zero provoca a criação de "furos" no dataset de imagens

        # Armazenamento da assinatura           
        endereco_destino = os.path.join(dir_data_final, "imagens", "simulacoes_"+str(lim_min_x)+"_"+str(lim_max_x)+"_"+str(lim_min_y)+"_"+str(lim_max_y))
        utils.cria_pasta(endereco_destino)                    
        cv2.imwrite(os.path.join(endereco_destino, processando + ".png"),grad_interpolado) 
        
        # Obtenção dos parâmetros dos defeitos: coordenadas, diâmetros e raios
        end_param_defeitos = os.path.join(endereco_defeitos, sim)
        param_defeitos = np.array(pd.read_csv(end_param_defeitos,header=None))
        
        
        [lin_int,col_int] = np.shape(grad_interpolado)
        
        interpolado_processado = np.zeros((lin_int,col_int,3),np.uint8) # É preciso transformar para uma imagem de 3 canais
                    
        interpolado_processado[:,:,0] = np.uint8(255*(1 - grad_interpolado/255)) # Inversão
        interpolado_processado[:,:,1] = np.uint8(255*(1 - grad_interpolado/255))
        interpolado_processado[:,:,2] = np.uint8(255*(1 - grad_interpolado/255))
        
        #Aplicação de transformações morfológicas                      
        mascara = interpolado_processado.copy()
        interpolado_processado = cv2.dilate(mascara,np.ones((1,1),np.uint8),iterations=1)
        interpolado_processado = cv2.Laplacian(interpolado_processado,cv2.CV_64F)

        tam_kernel = (9,9)
        interpolado_processado = cv2.GaussianBlur(interpolado_processado, tam_kernel,4,5)# Remoção de ruídos introduzidos pelo laplaciano
        interpolado_processado = np.uint8(interpolado_processado)

        shifted = cv2.pyrMeanShiftFiltering(np.uint8(interpolado_processado), 3,3)
        
        #Conversão para escala de cinza e aplicação do limiar de Otsu
        cinza = cv2.cvtColor(shifted, cv2.COLOR_BGR2GRAY)
        tam_kernel1 = (7,7)
        cinza = cv2.blur(cinza, tam_kernel1)
        limiar = cv2.threshold(cinza, 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

        
        # Salvar a imagem modificada
        endereco_destino = os.path.join(dir_data_final, "imagens", "filtro_"+str(lim_min_x)+"_"+str(lim_max_x)+"_"+str(lim_min_y)+"_"+str(lim_max_y))
        utils.cria_pasta(endereco_destino)
        cv2.imwrite(os.path.join(endereco_destino, processando + ".png"),np.uint8(cinza))
        
        # Cálculo da distância euclidiana exata de cada pixel binário até o pixel zero mais próximo
        # E obtenção dos picos de máximo nesse mapa de distância, tentativa de encontrar o centro das regiões
        
        # Criação de padding é necessária para permitir que a transformada euclidiana de distância 
        # encontre contorno fechado para defeitos nas bordas da imagem elétrica
        [lin_int_orig,col_int_orig] = np.shape(grad_interpolado)
        padding = np.zeros([lin_int_orig + tam_padding,col_int_orig + tam_padding])
        ajuste_padding = int(tam_padding/2)
        padding[ajuste_padding:ajuste_padding + lin_int_orig, ajuste_padding:ajuste_padding + col_int_orig] = limiar
        limiar = copy(padding)
        [lin_int,col_int] = np.shape(padding)


        D = ndimage.distance_transform_edt(limiar,sampling=[1,2])
        localMax = peak_local_max(D, indices=False, min_distance=3, labels=limiar)

        # Análise de componentes conectados nos picos locais, usando a conectividade 8
        # Aplicação do algoritmo Watershed
        marcadores = ndimage.label(localMax, structure=np.ones((3,3)))[0]
        labels = watershed(-D, marcadores, mask=limiar) #  supõe que marcadores representam mínimos locais
            
        # Obtenção dos pixels com mesmo rótulo, cada rótulo é uma região
        num_tot_reg_segmentadas = num_tot_reg_segmentadas + len(np.unique(labels)) - 1

#            Sobreposição dos círculos e de todo o processamento anterior
        auxiliar = np.zeros((lin_int,col_int,3),np.uint8) # É preciso transformar para uma imagem de 3 canais
        img_final = np.zeros((lin_int,col_int,3),np.uint8)
        lista_cada_img = []

        for label in np.unique(labels):
            # Se label = 0, o fundo da imagem está sendo analisado
            if label == 0:
                continue
            
            # Atribuição dos rótulos
            mascara = np.zeros(padding.shape, dtype="uint8")
            mascara[labels == label] = 1 # Segundo a documentação de findContours, é melhor usar uma imagem binária
            auxiliar[:,:,0] = auxiliar[:,:,1] = auxiliar[:,:,2] = mascara.copy()
            img_final = img_final + 255*auxiliar
            
            try:
            # Detecção dos contornos e armazenamento dos maiores deles
                cnts = cv2.findContours(mascara.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                cnts = imutils.grab_contours(cnts)
                c = max(cnts, key=cv2.contourArea)
            except:
                    print("Nenuhm defeito encontrado")
            else:
                # Encontra o menor círculo que cobre a área desejada
                ((_), raio) = cv2.minEnclosingCircle(c) 
                M = cv2.moments(c) # Características do círculo encontrado
                        
                if M["m00"] != 0:
                    centro = ((M["m10"]/M["m00"]), (M["m01"]/M["m00"]))
                    cv2.drawContours(img_final, cnts, -1, (255,0,0), 1)
                    cv2.putText(img_final, "{}".format(label), (int(centro[0]) -3, int(centro[1]+3)),cv2.FONT_HERSHEY_SCRIPT_SIMPLEX , 0.3, (255, 0, 0), 1)
                    
                        # Salvar a imagem segmentada com rótulos, sem padding, na mesma dimensão da entrada            
                    endereco_destino = os.path.join(dir_data_final, "imagens", "segmentacao_"+str(lim_min_x)+"_"+str(lim_max_x)+"_"+str(lim_min_y)+"_"+str(lim_max_y))
                    utils.cria_pasta(endereco_destino)             
                    cv2.imwrite(os.path.join(endereco_destino, processando + ".png"),(img_final[ajuste_padding:ajuste_padding + lin_int_orig, ajuste_padding:ajuste_padding + col_int_orig])) 
    
                    # Obtenção das ROIs
                    """ Ajuste para as bordas da segmentação, caixas com tamanho fixo
                        e fazer o centro do quadrado coincidir com o centro do defeito """
                    X = int(centro[0])
                    Y = int(centro[1])
                    H = W = int(tam_caixa/2)
                    
                    if X+W > len(padding):
                        diferencaX = X+W - len(padding)
                        X = X - diferencaX
                        
                    if Y+H > len(padding):
                        diferencaY = Y+H - len(padding)
                        Y = Y - diferencaY
                        
                    if X-W < 0:
                        X = X + np.abs(X-W)
                    
                    if Y-H < 0:
                        Y = Y + np.abs(Y-H)
                        
                    roi = np.zeros((lin_int,col_int))
                    tmp = np.zeros((lin_int,col_int))
                    tmp[ajuste_padding:ajuste_padding + lin_int_orig, ajuste_padding:ajuste_padding + col_int_orig] = grad_interpolado

                    ind = np.where(labels == label)
                    roi[ind] = tmp[ind] # Obtém apenas a região segmentada do gradiente
                                    
        
                    # Correspondência entre as coordenadas dos defeitos encontrados e dos defeitos simulados: conversão de pixel para mm
                    regressao = stats.linregress(np.arange(0,len(grad_interpolado),1),eixo_x)
                    ajuste_x = np.round(regressao[0])*(centro[0]-ajuste_padding) + np.round(regressao[1])  
    
                    regressao1 = stats.linregress(np.flip(np.arange(0,len(grad_interpolado),1)),eixo_y) # eixo y é decrescente
                    ajuste_y =  np.round(regressao1[0])*(centro[1]-ajuste_padding) +  np.round(regressao1[1])          
    
                    dist_lst=[]
                    dist_mat = np.zeros([len(param_defeitos[:,2])-1])
                    
                    for q in range(0,len(param_defeitos)-1):
                        x_simulacao = np.float(param_defeitos[q+1,2])
                        y_simulacao = np.float(param_defeitos[q+1,3])
                        distancia = utils.calculo_distancia(ajuste_x,ajuste_y,x_simulacao,y_simulacao)
                        dist_mat[q] = distancia
    #                        print(str(distancia))
                    dist_lst.append(dist_mat) 
                    menor_dist = np.min(dist_lst)
                    """ Útil para verificar a distância de uma roi incorretamente encontrada, não existe defeito nesta região """
                    debug.append(dist_lst) 
                    
                    tmp[ajuste_padding:ajuste_padding + lin_int_orig, ajuste_padding:ajuste_padding + col_int_orig] = assinatura_ref

                    #Verficação da porcentagem
                    if menor_dist < limiar_menor_dist: # Se a distância for maior que o limiar, foi detectada uma região inválida
                        assinatura = tmp[int(centro[1]),int(centro[0])]
                        if assinatura == 0:
                            sys.exit("\033[1;31;47m Impossível prosseguir")                                   
                        indice_area_real = np.array(np.where(dist_lst == np.min(dist_lst)))[1,0]
                        area_real = (np.float(param_defeitos[indice_area_real+1,0])) # +1 pra excluir o cabeçalho
                        prof_real = (np.float(param_defeitos[indice_area_real+1,1])) 

                        """Determinação das classes"""
            
                        if(area_real < 8):
                            classe_astm = 0
                        elif(area_real >= 8 and area_real < 12.5):
                            classe_astm = 1
                        elif(area_real >= 12.5 and area_real < 24.5):
                            classe_astm = 2
                        elif(area_real >= 24.5):
                            classe_astm = 3
                                
                        area_bbox = []
                        num_ROI += 1
                        endereco_destino = os.path.join(dir_data_final, "roi_dataset", str(classe_astm))
                        utils.cria_pasta(endereco_destino)
                        imgs_salvas = os.listdir(endereco_destino)
                        num_imgs_salvas = len(imgs_salvas)
                        
                        # Salvando as roi's com regiões segmentadas em escala de cinza
                        roi_tmp = roi[Y-H:Y+H, X-W:X+W] 
                        data = np.zeros(np.shape(roi_tmp), dtype = np.float32)
                        indices = np.where(roi_tmp > 0) # Apenas os valores da região segmentada
                        data[indices] = 255 - roi_tmp[indices] # Inversão para o centro tender ao valor máximo
                        cv2.imwrite(os.path.join(endereco_destino, "classe" + "_" + str(classe_astm) + "_" + str(num_imgs_salvas+1).zfill(4) + ".png"),np.array(data,dtype = np.float32))

                        # Criação dos targets das roi's
                        endereco_destino_target = os.path.join(dir_data_final, "target", str(classe_astm))
                        utils.cria_pasta(endereco_destino_target)
                        
                        coord_x = tam_caixa/2 # offset para centralizar o defeito com a imagem
                        coord_y = tam_caixa/2
                        
                        img_alta_resolucao = np.arange(0,tam_caixa,.1), np.arange(0,tam_caixa,.1) # alta resolução para maior definição dos defeitos
                        x_x,y_y = np.meshgrid(img_alta_resolucao[0],img_alta_resolucao[1])
                                                
                        circulo = np.array((x_x - coord_x)**2 + (y_y - coord_y)**2 <= prof_real**2,dtype = np.int8)
                            
                        nome_arquivo = os.path.join(endereco_destino_target, "classe" + "_" + str(classe_astm) + "_" + str(num_imgs_salvas+1).zfill(4) + ".png")
                        img = Image.fromarray(255*np.uint8(circulo)).resize((tam_caixa,tam_caixa))
                        img.save(nome_arquivo)


                        area_pct = []
                        
                        for j in range(0, len(porcentagem)):        
                            fatia = np.uint8(np.array((roi[Y-H:Y+H, X-W:X+W] >= porcentagem[j]*maximo_astm_referencia*255))) # np.invert??                    
                            resultado = np.zeros([len(padding),len(padding)]).astype(np.uint8)                                          
                            # Cópia da imagem para a região da matriz de acordo com os intervalos calculados
                            resultado[Y-H:Y+H, X-W:X+W] = 255*np.array(fatia)
    
                            area_indiv = np.ma.size(np.where(np.array(resultado)[:,:] == 255),1) # Contagem do num de pixels brancos, área segmentada
                                
                            area_bbox.append(area_indiv)
                        area_bbox.insert(len(area_bbox)+1,raio)
                        area_bbox.insert(len(area_bbox)+1,assinatura)
                        area_bbox.insert(len(area_bbox)+1,area_real)
                        area_bbox.insert(len(area_bbox)+1,prof_real)
                        area_bbox.insert(len(area_bbox)+1,classe_astm)
                        area_pct.append(area_bbox)                            
                        area_roi.append(area_pct)
                        num_ROI = 0 
                                                
                # Exibição de figuras
                    if exibicao_seg != 0:
                        fig,ax = plt.subplots(2,2)
                        ax = ax.flatten()
                        ax[0].imshow(grad_interpolado)
                        ax[0].set_title("Original")
                        ax[1].imshow(shifted)
                        ax[1].set_title("Shifted")
                        ax[2].imshow(limiar)
                        ax[2].set_title("Limiar")
                        ax[3].imshow(img_final)
                        ax[3].set_title("Segmentado")
                        plt.show()
    area_classe.append(area_roi)

    ''' Organização dos dados e criação do arquivo csv '''

    utils.gera_csv(nome_csv, area_classe, porcentagem)

if __name__ == '__main__':
    main()