# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 10:18:04 2020

@author: Leandro
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,plot_confusion_matrix
import os
from joblib import load
import sys
cur_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.split(cur_dir)[0])
import utils



# Load dataset
url_train = "D:\\Google Drive\\Datasets\\v4\\training.csv"
url_test = "D:\\Google Drive\\Datasets\\v4\\test.csv"

normalization = 0
exclui_corr = 0

def main():

    limiar = .92
    train = pd.read_csv(url_train)
    test = pd.read_csv(url_test)

    Y_train = np.array(train['classe'])
    X_train = train.drop(columns = ['classe','area_real','prof_real'])
    
    Y_test = np.array(test['classe'])
    X_test = test.drop(columns = ['classe','area_real','prof_real'])
    
    
    corr = X_train.corr()
    fig, ax = plt.subplots(figsize=(10, 10))
    sn.heatmap(corr, annot=True, fmt=".2f")
    ax.set_title("Correlação")
    plt.show()
    
    
    if exclui_corr == 1:
        X_train,atr_excluido = utils.correlation(X_train,limiar)
        X_test = X_test.drop([columns for columns in atr_excluido],axis=1)
        print(str(atr_excluido) + " foram excluídos")
        pt2 = "com_sel"
        titulo = "Com Seleção"
    else:
        pt2 = "sem_sel"
        titulo = "Sem Seleção"
            
    if normalization == 1: 
        scaler = StandardScaler().fit(X_train)    
        X_train = scaler.transform(X_train)
        scaler = StandardScaler().fit(X_test) 
        X_test = scaler.transform(X_test)
        pt1 = "com_norm_"
        titulo = titulo + " e com Normalização"
    
    else:
        pt1 = "sem_norm_"
        titulo = titulo + " e sem Normalização"
    
    
    
    modelos = load("best_models_" + pt1 + pt2 + ".dat")
    
    pasta = os.path.join(pt1 + pt2 +"_figuras")
    utils.cria_pasta(pasta)
    
    dpi = 120
    figsize = None
    nomes = ['Regressão Logística','SGD','Árvore de Decisão',
             'k-Vizinhos Mais Próximos','Vetor de Suporte','AdaBoost',
             'Gradient Boosting','Floresta Aleatória']
    pred = []
    for modelo,nome in zip(modelos,nomes):
        print("\n" + nome)
        print("Acurácia treino {:.3f}%".format(100*modelo.score(X_train,Y_train)))
        print("Acurácia teste {:.3f}%".format(100*modelo.score(X_test,Y_test)))
        print("Importância das variáveis")
        try:
            z = modelo.coef_
            print(z)
        except:
            pass
        try:
            z=modelo.feature_importances_
            print(z)
        except:
            pass
        predictions = modelo.predict(X_test)
        pred.append(predictions)
        # print("Matriz de confusão")
        tn, fp, fn, tp = confusion_matrix(y_true=Y_test, y_pred=predictions)
        # print(confusion_matrix(y_true=Y_validation, y_pred=predictions))
        fig = plt.figure(dpi = dpi, figsize = figsize)
        ax = fig.add_subplot(111)
        mat_conf = plot_confusion_matrix(estimator = modelo, X = X_test, y_true = Y_test, ax = ax, cmap = 'Blues')
        mat_conf.im_.colorbar.remove()
        ax.set_title(str(nome))
        ax.set_ylabel('Classe real')
        ax.set_xlabel('Classe predita')
        labels = ['0','1','2','3']
        ax.xaxis.set_ticklabels(labels)
        ax.yaxis.set_ticklabels(labels)
        plt.show()
        fig.savefig(os.path.join(pasta,nome+" Matriz de confusão"), transparent=True)

if __name__=="__main__":
    main()