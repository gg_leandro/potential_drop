# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)




import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from joblib import dump
import sys
import os
cur_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.split(cur_dir)[0])
import utils

""" Grid search para classificação da severidade dos defeitos.
    É feita a busca de parametros para regressão logística,
    árvore de decisão, KNN, SVM, gradient boosting e random forest"""

# Início configurações

# Leitura do dataset
url_train = "F:\\Google Drive\\Datasets\\v4\\training.csv"
url_test = "F:\\Google Drive\\Datasets\\v4\\test.csv"

# Normalização e seleção de atributos: 1 - ativa; 0 - desativa
normalization = 0
exclui_corr = 1
limiar = .92 # limite de correlação
num_folds = 5
scoring = "accuracy"
# Fim configurações


def main():
    seed = 7
    train = pd.read_csv(url_train)
    test = pd.read_csv(url_test)
    
    
    Y_train = np.array(train['classe'])
    X_train = train.drop(columns = ['classe','area_real','prof_real']) # Exclusão dos atributos usados para regressão
    
    
    Y_test = np.array(test['classe'])
    X_test = test.drop(columns = ['classe','area_real','prof_real'])
    
    
    corr = X_train.corr()
    fig, ax = plt.subplots(figsize=(10, 10))
    sn.heatmap(corr, annot=True, fmt=".2f")
    ax.set_title("Correlação")
    plt.show()
    
    
    if exclui_corr == 1:
        X_train,atr_excluido = utils.correlation(X_train,limiar)
        X_test = X_test.drop([columns for columns in atr_excluido],axis=1)
        print(str(atr_excluido) + " foram excluídos")
        pt2 = "com_sel"
        titulo = "Com Seleção"
    else:
        pt2 = "sem_sel"
        titulo = "Sem Seleção"
           
    
    if normalization == 1:
        scaler = StandardScaler().fit(X_train)    
        X_train = scaler.transform(X_train)
        scaler = StandardScaler().fit(X_test) 
        X_test = scaler.transform(X_test)
        pt1 = "com_norm_"
        titulo = titulo + " e com Normalização"
    
    else:
        pt1 = "sem_norm_"
        titulo = titulo + " e sem Normalização"
    
    
    
    
    # Treinamento
    print("_________Treino_________")
        
    
    kfold = KFold(n_splits=num_folds, random_state=seed)
    best = []
    results = []
    
    # Reg log
    max_iter = [200,400,800,1600]
    tol = [1e-5,1e-6]
    param_grid = dict(max_iter=max_iter,tol=tol)
    grid = GridSearchCV(estimator=LogisticRegression(), param_grid=param_grid, scoring=scoring, cv=kfold)
    grid_result = grid.fit(X_train, Y_train)
    best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    results.append(means)
    
    
    
    # Árvore de decisão
    profundidade = [2, 4, 6, 8, 10]
    criterion = ["gini", "entropy"]
    splitter = ["best", "random"]
    param_grid = dict(max_depth=profundidade, criterion=criterion, splitter=splitter)
    grid = GridSearchCV(estimator=DecisionTreeClassifier(), param_grid=param_grid, scoring=scoring, cv=kfold)
    grid_result = grid.fit(X_train, Y_train)
    best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    results.append(means)
    
    
    # kNN
    k = [1, 3, 5, 7, 9, 11, 13, 15]
    param_grid = dict(n_neighbors = k)
    grid = GridSearchCV(estimator=KNeighborsClassifier(), param_grid=param_grid, scoring=scoring, cv=kfold)
    grid_result = grid.fit(X_train, Y_train)
    best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    results.append(means)
    
    
    
    # SVM
    c_values = [0.1, 0.3, 0.5, 0.7, 0.9, 1.0, 1.3, 1.5, 1.7, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0]
    # gamma = [0.1, 0.3, 0.5, 0.7, 0.9]
    kernel_values = ['linear', 'rbf']
    param_grid = dict(C=c_values, kernel=kernel_values)
    svm = SVC(max_iter = 8000) # limitação necessária para evitar a não convergência
    grid = GridSearchCV(estimator=svm, param_grid=param_grid, scoring=scoring, cv=kfold)
    grid_result = grid.fit(X_train, Y_train)
    best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    svm = grid.best_estimator_
    results.append(means)
    
    
    
    # GradientBoosting
    eta = [.001, .01, .05, .1, .3, .4]
    n_estimators =[50, 100, 150, 200, 250, 300, 500, 1000]
    param_grid = dict(learning_rate=eta, n_estimators=n_estimators)
    
    grid = GridSearchCV(estimator=GradientBoostingClassifier(), param_grid=param_grid, scoring=scoring, cv=kfold)
    grid_result = grid.fit(X_train, Y_train)
    best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    results.append(means)
     
    
    # RandomForest
    profundidade = [2, 4, 6, 8, 10]
    num_estim = [80, 100, 200, 400, 600, 800, 1000]
    param_grid = dict(max_depth=profundidade,  n_estimators = num_estim)
    grid = GridSearchCV(estimator=RandomForestClassifier(), param_grid=param_grid, scoring=scoring, cv=kfold)
    grid_result = grid.fit(X_train, Y_train)
    best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stds = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stds, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    results.append(means)
    
    
    
    ## Treinamento com os melhores parâmetros obtidos
    modelos = []
    
    modelos.append(LogisticRegression(max_iter=list(best[0][2].values())[0], tol = list(best[0][2].values())[1]))
    modelos.append(SGDClassifier(early_stopping=list(best[1][2].values())[0], loss=list(best[1][2].values())[1], max_iter=list(best[1][2].values())[2], n_iter_no_change=list(best[1][2].values())[3]))
    modelos.append(DecisionTreeClassifier(criterion=list(best[2][2].values())[0], max_depth=list(best[2][2].values())[1], splitter=list(best[2][2].values())[2]))
    modelos.append(KNeighborsClassifier(n_neighbors = list(best[3][2].values())[0]))
    modelos.append(SVC(C=list(best[4][2].values())[0], kernel=list(best[4][2].values())[1]))
    modelos.append(AdaBoostClassifier(learning_rate=list(best[5][2].values())[0],n_estimators = list(best[5][2].values())[1]))
    modelos.append(GradientBoostingClassifier(learning_rate=list(best[6][2].values())[0], n_estimators=list(best[6][2].values())[1]))
    modelos.append(RandomForestClassifier(max_depth=list(best[7][2].values())[0],n_estimators=list(best[7][2].values())[1]))
    
    for modelo in modelos:
        modelo.fit(X_train,Y_train)
        print("\n"+str(modelo))
        print("Acurácia treino {:.3f}%".format(100*modelo.score(X_train,Y_train)))
        print("Acurácia teste {:.3f}%".format(100*modelo.score(X_test,Y_test)))
    
        
    # Salvando os modelos obtidos
    dump(modelos, "best_models_" + pt1 + pt2 + ".dat") 
    
if __name__=="__main__":
    main()