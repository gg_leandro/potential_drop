
# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix,ConfusionMatrixDisplay,classification_report
from joblib import load
import sys
import os
cur_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.split(cur_dir)[0])
import utils


""" Processa os resultados das classificações"""

# Início configurações

# Leitura do dataset
url_train = "D:\\Google Drive\\Datasets\\v4\\training.csv"
url_test = "D:\\Google Drive\\Datasets\\v4\\test.csv"

# Normalização e seleção de atributos: 1 - ativa; 0 - desativa
normalization = 0
exclui_corr = 0

#Onde os resultados são salvos
nome_pasta = "resultados"
# Fim configurações

def main():
    train = pd.read_csv(url_train)
    test = pd.read_csv(url_test)
    
    
    Y_train = np.array(train['classe'])
    X_train = train.drop(columns = ['classe','area_real','prof_real'])
    
    Y_test = np.array(test['classe'])
    X_test = test.drop(columns = ['classe','area_real','prof_real'])
    
    
    corr = X_train.corr()
    fig, ax = plt.subplots(figsize=(10, 10))
    sn.heatmap(corr, annot=True, fmt=".2f")
    ax.set_title("Correlação")
    plt.show()
    limiar = .92
    
    if exclui_corr == 1:
        X_train,atr_excluido = utils.correlation(X_train,limiar)
        X_test = X_test.drop([columns for columns in atr_excluido],axis=1)
        print(str(atr_excluido) + " foram excluídos")
        pt2 = "com_sel"
        titulo = "Com Seleção"
    else:
        pt2 = "sem_sel"
        titulo = "Sem Seleção"
        
    
    if normalization == 1:
        scaler = StandardScaler().fit(X_train)    
        X_train = scaler.transform(X_train)
        scaler = StandardScaler().fit(X_test) 
        X_test = scaler.transform(X_test)
        pt1 = "com_norm_"
        titulo = titulo + " e com Normalização"
    
    else:
        pt1 = "sem_norm_"
        titulo = titulo + " e sem Normalização"
    
    
    # Carregando os modelos obtidos
    best_modelos = load("best_models_" + pt1 + pt2 + ".dat")
    
    pasta = os.path.join(nome_pasta, pt1 + pt2)
    utils.cria_pasta(pasta)
    
    
    dpi = 150
    figsize = (4,4)
    
    modelos = []
    
    modelos.append(best_modelos[0])
    modelos.append(best_modelos[2])
    modelos.append(best_modelos[3])
    modelos.append(best_modelos[4])
    modelos.append(best_modelos[6])
    modelos.append(best_modelos[7])
    
    # Geração de métricas e figuras
    nomes = ['Regressão Logística','Árvore de Decisão','k-Vizinhos Mais Próximos',
             'Vetor de Suporte','Gradient Boosting','Random Forest']
    
    recall_classe_lista = [] 
    recall_test_list = []
    erro_grave_list = []
    erro_nao_grave_lista = []
    for modelo,nome in zip(modelos,nomes):
          
        print(nome)
        
    
        recall_test = modelo.score(X_test,Y_test)
        recall_test_list.append(recall_test)
        
        y_pred = modelo.predict(X_test)
        
        conf = confusion_matrix(y_true=Y_test, y_pred=y_pred)
        recall_por_classe = conf.diagonal()/conf.sum(axis=1)
        recall_classe_lista.append(recall_por_classe)
        
        l1, l2, l3, l4 = confusion_matrix(y_true=Y_test, y_pred=y_pred)
        erro_grave = l2[0] + sum(l3[0:2])  + sum(l4[0:3])
        erro_grave_list.append(erro_grave)
        erro_nao_grave = sum(l1[1:4]) + sum(l2[2:4]) + l3[3]
        erro_nao_grave_lista.append(erro_nao_grave)
            
           
        print(classification_report(y_true=Y_test, y_pred=y_pred,digits=4))
        
        fig = plt.figure(dpi = dpi, figsize = figsize)
        labels = ['B2','B3','B4','B5']
        ax = fig.add_subplot(111)
        matriz_conf = ConfusionMatrixDisplay(conf,display_labels=labels)
        matriz_conf.plot(cmap = 'Blues', ax=ax,values_format ='g')
        matriz_conf.im_.colorbar.remove()
        ax.set_title('Matriz de confusão');
        ax.set_ylabel('Classe real')
        ax.set_xlabel('Classe predita') 
        ax.xaxis.set_ticklabels(labels)
        ax.yaxis.set_ticklabels(labels)
        plt.show()
        fig.savefig(os.path.join(pasta,nome+" Matriz_conf.eps"), transparent=True)
    
        del modelo
        
    
    
    parte1 = pd.DataFrame([recall_test_list, erro_grave_list, erro_nao_grave_lista])
    parte1.columns = nomes
    parte1.index = ["acuracia_test","erro_grave","erro_nao_grave"]
    
    parte2 = pd.DataFrame([recall_classe_lista[0],recall_classe_lista[1],recall_classe_lista[2],
                           recall_classe_lista[3],recall_classe_lista[4],recall_classe_lista[5]]).T
    parte2.columns = nomes
    parte2.index = ["recall_B2","recall_B3","recall_B4","recall_B5"]
    
    parte3 = pd.DataFrame([recall_classe_lista[0].mean(),recall_classe_lista[1].mean(),recall_classe_lista[2].mean(),
                           recall_classe_lista[3].mean(),recall_classe_lista[4].mean(),recall_classe_lista[5].mean()]).T
    parte3.columns = nomes
    parte3.index = ["recall_media"]
    
    final = pd.concat([parte1,parte2,parte3])
    final.to_excel(os.path.join(pasta,'metricas.xlsx'),float_format = '%.5e') 
    
if __name__=="__main__":
    main()
    
    
    
    
    
    
    
    
    
    
    
