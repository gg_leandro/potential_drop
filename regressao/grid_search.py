# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)

""" Grid search para regressão: estimativa da profundidade dos defeitos.
é feita a busca de parametros para árvore de decisão, KNN, SVR, gradient boosting e random forest"""

# Início configurações

# Leitura do dataset
url_train = "F:\\Google Drive\\Datasets\\v4\\training.csv"
url_test = "F:\\Google Drive\\Datasets\\v4\\test.csv"

# Normalização e seleção de atributos: 1 - ativa; 0 - desativa
normalization = 0
exclui_corr = 1
limiar = .92 # limite de correlação

# Fim configurações



import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import RandomForestRegressor
from joblib import dump
import sys
import os
cur_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.split(cur_dir)[0])
import utils



train = pd.read_csv(url_train)
test = pd.read_csv(url_test)


Y_train = np.array(train['prof_real'])
train = train.drop(columns = ['prof_real'])

Y_test = np.array(test['prof_real'])
test = test.drop(columns = ['prof_real'])

# Exclusão dos atributos usados para classificação
X_train = train.drop(columns = ['classe','area_real'])
X_test = test.drop(columns = ['classe','area_real'])


corr = train.corr()
fig, ax = plt.subplots(figsize=(10, 10))
sn.heatmap(corr, annot=True, fmt=".2f")
ax.set_title("Correlação")
plt.show()

# Execução da seleção de atributos
if exclui_corr == 1:
    X_train,atr_excluido = utils.correlation(X_train,limiar)
    X_test = X_test.drop([columns for columns in atr_excluido],axis=1)
    print(str(atr_excluido) + " foram excluídos")
    pt2 = "com_sel"
    titulo = "Com Seleção"
else:
    pt2 = "sem_sel"
    titulo = "Sem Seleção"


# Execução da normalização
if normalization == 1: 
    scaler = StandardScaler().fit(X_train)    
    X_train = scaler.transform(X_train)
    scaler = StandardScaler().fit(X_test) 
    X_test = scaler.transform(X_test)
    pt1 = "com_norm_"
    titulo = titulo + " e com Normalização"

else:
    pt1 = "sem_norm_"
    titulo = titulo + " e sem Normalização"

# Treinamento
print("_________Treino_________")

seed = 7    
num_folds = 5
scoring = 'neg_mean_squared_error'
kfold = KFold(n_splits=num_folds, random_state=seed)
best = []
results = []

# Reg linear
normalize = [False] # Com parametros padrões, aqui apenas para completar o fluxo de trabalho
param_grid = dict(normalize=normalize)
grid = GridSearchCV(estimator=LinearRegression(), param_grid=param_grid, scoring=scoring, cv=kfold)
grid_result = grid.fit(X_train, Y_train)
best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
results.append(means)


# Árvore de decisão
profundidade = [2, 4, 6, 8, 10]
criterion = ["mae"]
splitter = ["best"]
param_grid = dict(max_depth=profundidade, criterion=criterion, splitter=splitter)
grid = GridSearchCV(estimator=DecisionTreeRegressor(), param_grid=param_grid, scoring=scoring, cv=kfold)
grid_result = grid.fit(X_train, Y_train)
best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
results.append(means)


# kNN
k = [1, 3, 5, 7, 9, 11, 13, 15]
param_grid = dict(n_neighbors = k)
grid = GridSearchCV(estimator=KNeighborsRegressor(), param_grid=param_grid, scoring=scoring, cv=kfold)
grid_result = grid.fit(X_train, Y_train)
best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
results.append(means)



# SVR
c_values = [0.1, 0.3, 0.5, 0.7, 0.9, 1.0, 1.3, 1.5, 1.7, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0]
kernel_values = ['linear', 'rbf']
param_grid = dict(C=c_values, kernel=kernel_values)
svrr = SVR(max_iter=3000) # limitação necessária para evitar a não convergência
grid = GridSearchCV(estimator=svrr, param_grid=param_grid, scoring=scoring, cv=kfold)
grid_result = grid.fit(X_train, Y_train)
best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
results.append(means)



# GradientBoosting
eta = [.001, .01, .05, .1, .3, .4]
n_estimators =[50, 100, 150, 200, 250, 300, 500, 1000]
param_grid = dict(learning_rate=eta, n_estimators=n_estimators)

grid = GridSearchCV(estimator=GradientBoostingRegressor(), param_grid=param_grid, scoring=scoring, cv=kfold)
grid_result = grid.fit(X_train, Y_train)
best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
results.append(means)
 

# RandomForest
profundidade = [2, 4, 6, 8, 10]
num_estim = [80, 100, 200, 400, 600, 800, 1000]
param_grid = dict(max_depth=profundidade,  n_estimators = num_estim)
grid = GridSearchCV(estimator=RandomForestRegressor(), param_grid=param_grid, scoring=scoring, cv=kfold)
grid_result = grid.fit(X_train, Y_train)
best.append([grid.best_estimator_, grid_result.best_score_, grid_result.best_params_])
print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
means = grid_result.cv_results_['mean_test_score']
stds = grid_result.cv_results_['std_test_score']
params = grid_result.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print("%f (%f) with: %r" % (mean, stdev, param))
results.append(means)



## Treinamento com os melhores parâmetros obtidos
modelos = []

modelos.append(LinearRegression(normalize=list(best[0][2].values())[0]))
modelos.append(DecisionTreeRegressor(criterion=list(best[1][2].values())[0],
                                     max_depth=list(best[1][2].values())[1], splitter=list(best[1][2].values())[2]))
modelos.append(KNeighborsRegressor(n_neighbors = list(best[2][2].values())[0]))
modelos.append(SVR(C=list(best[4][2].values())[0], kernel=list(best[3][2].values())[1]))
modelos.append(GradientBoostingRegressor(learning_rate=list(best[4][2].values())[0], n_estimators=list(best[4][2].values())[1]))
modelos.append(RandomForestRegressor(max_depth=list(best[5][2].values())[0],n_estimators = list(best[5][2].values())[1]))


erro_lista = []
erro_max_lista = []
erro_media_lista = []
erro_mediana_lista = []
for modelo in modelos:
    modelo.fit(X_train,Y_train)
    print("\n"+str(modelo))
    print("R² treino {:.5f}".format(modelo.score(X_train,Y_train)))
    y_pred_train = modelo.predict(X_train)
    print("MSE treino {:e}".format(mean_squared_error(Y_train, y_pred_train)))
    print("R² teste {:.5f}".format(modelo.score(X_test,Y_test)))
    y_pred = modelo.predict(X_test)
    print("MSE teste {:e}".format(mean_squared_error(Y_test, y_pred)))
    
    erro = abs(Y_test - y_pred)
    erro_max = max(erro)
    erro_media = np.mean(erro)
    erro_mediana = np.median(erro)
    
    ind_maior_erro = int(np.where(erro == erro_max)[0])
    print('Maior erro ocorre em: ',ind_maior_erro)
    max_pct = 100*abs(Y_test[ind_maior_erro] - y_pred[ind_maior_erro])/Y_test[ind_maior_erro] # erro maximo percentual
    erro_lista.append(erro)
    erro_max_lista.append(erro_max)
    erro_media_lista.append(erro_media)
    erro_mediana_lista.append(erro_mediana)
   
    fig = plt.figure(dpi = 150)
    ax = fig.add_subplot(111)
    ax.set_ylabel('Profundidade (mm)')
    ax.set_xlabel('Amostra')
    ax.set_title(str(modelo))
    plt.scatter(np.arange(0,len(erro),1 ),Y_test,marker='o',label='Teste')
    plt.scatter(np.arange(0,len(erro),1 ),y_pred,marker='+',label='Predição')
    ax.legend(loc='best')
    plt.grid()
    plt.show()
       
    fig = plt.figure(dpi = 150)
    ax = fig.add_subplot(111)
    ax.set_ylabel('Erro absoluto (mm)')
    ax.set_xlabel('Amostra')
    ax.set_title(str(modelo))
    plt.scatter(np.arange(0,len(erro),1 ),erro,marker='o')
    plt.grid()
    plt.show()
       
    fig = plt.figure(dpi = 150)
    ax = fig.add_subplot(111)
    ax.set_ylabel('Frequência')
    ax.set_xlabel('Erro absoluto (mm)')
    ax.set_title(str(modelo))
    plt.hist(erro)
    plt.grid()
    plt.show()

# Salvando os modelos obtidos
dump(modelos, "best_models_" + pt1 + pt2 + ".dat") 

