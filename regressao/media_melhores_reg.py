# import warnings filter
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)

""" Processa os resultados das regressões"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error
from joblib import load
import os
import sys
cur_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.split(cur_dir)[0])
import utils

# Início configurações

# Leitura do dataset
url_train = "D:\\Google Drive\\Datasets\\v4\\training.csv"
url_test = "D:\\Google Drive\\Datasets\\v4\\test.csv"

# Normalização e seleção de atributos: 1 - ativa; 0 - desativa
normalization = 0
exclui_corr = 0
limiar = .92 # limite de correlação

#Onde os resultados são salvos
nome_pasta = "resultados__"
# Fim configurações

def main():
    train = pd.read_csv(url_train)
    test = pd.read_csv(url_test)
    
    
    Y_train = np.array(train['prof_real'])
    train = train.drop(columns = ['prof_real'])
    
    Y_test = np.array(test['prof_real'])
    test = test.drop(columns = ['prof_real'])
    
    # Exclusão dos atributos usados para classificação
    X_train = train.drop(columns = ['classe','area_real'])
    X_test = test.drop(columns = ['classe','area_real'])
    
    
    corr = train.corr()
    fig, ax = plt.subplots(figsize=(10, 10))
    sn.heatmap(corr, annot=True, fmt=".2f")
    ax.set_title("Correlação")
    plt.show()
    
    # Execução da seleção de atributos
    if exclui_corr == 1:
        X_train,atr_excluido = utils.correlation(X_train,limiar)
        X_test = X_test.drop([columns for columns in atr_excluido],axis=1)
        print(str(atr_excluido) + " foram excluídos")
        pt2 = "com_sel"
        titulo = "Com Seleção"
    else:
        pt2 = "sem_sel"
        titulo = "Sem Seleção"
    
    
    # Execução da normalização
    if normalization == 1: 
        scaler = StandardScaler().fit(X_train)    
        X_train = scaler.transform(X_train)
        scaler = StandardScaler().fit(X_test) 
        X_test = scaler.transform(X_test)
        pt1 = "com_norm_"
        titulo = titulo + " e com Normalização"
    
    else:
        pt1 = "sem_norm_"
        titulo = titulo + " e sem Normalização"
    
    
    # Carregando os modelos obtidos
    best_modelos = load("best_models_" + pt1 + pt2 + ".dat") 
    
    modelos = []
    
    modelos.append(best_modelos[0])
    modelos.append(best_modelos[1])
    modelos.append(best_modelos[2])
    modelos.append(best_modelos[3])
    modelos.append(best_modelos[4])
    modelos.append(best_modelos[5])
    
    X_test = np.delete(np.array(X_test),500,axis=0) # Exclusão do exemplo com erro exagerado no gradboosting e random forest
    Y_test = np.delete(np.array(Y_test),500,axis=0)
    
    pasta = os.path.join(nome_pasta, pt1 + pt2)
    utils.cria_pasta(pasta)
    
    dpi = 120
    figsize = (5,4)
    
    
    # Geração de métricas e figuras
    nomes = ['Regressão Linear','Árvore de Decisão','k-Vizinhos Mais Próximos',
             'Vetor de Suporte','Gradient Boosting','Random Forest']
    linhas = ["mse","r2","erro_abs_max","erro_abs_medio"]
    mse = []
    R2_lista = []
    erro_abs_lista = []
    erro_max_lista = []
    erro_medio_lista = []
    relacao_lista = []
    y_pred_lista = []
    
    for modelo,nome in zip(modelos,nomes):
        
        print(nome)
    
        y_pred = modelo.predict(X_test)
        erro = mean_squared_error(Y_test, y_pred)
        mse.append(erro)
        
        R2 = modelo.score(X_test,Y_test)
        R2_lista.append(R2)
         
        erro = abs(Y_test - y_pred)
        erro_max_lista.append(max(erro))
        erro_medio_lista.append(np.mean(erro))
        
        relacao_lista.append(y_pred/Y_test)
        
        y_pred_lista.append(y_pred)      
        erro_abs_lista.append(erro)
        
        del modelo
            
    
      
        y_pred_array = np.array(y_pred_lista).mean(axis = 0)
        
        erro_abs_array = np.array(erro_abs_lista)
        erro_abs_med = erro_abs_array.mean(axis = 0)
        histt = np.array(relacao_lista).mean(axis = 0)
        med_histt = np.mean(histt)
        dev_histt = np.std(histt)
    
        
        
        fig = plt.figure(dpi = dpi, figsize = figsize)
        ax = fig.add_subplot(111)
        ax.set_ylabel('Profundidade (mm)')
        ax.set_xlabel('Amostra')
        ax.set_title("Comparação Predição x Teste")
        plt.tight_layout()
        plt.scatter(np.arange(0,len(y_pred_array),1 ),Y_test,marker='o',label='Teste')
        plt.scatter(np.arange(0,len(y_pred_array),1 ),y_pred_array,marker='+',label='Predição')
        ax.legend(loc='best')
        plt.grid()
        plt.show()
        fig.savefig(os.path.join(pasta,nome+" Comparação Predição x Teste.eps"), transparent=True)
        
    
        
        fig = plt.figure(dpi = dpi, figsize = figsize)
        ax = fig.add_subplot(111)
        ax.set_ylabel('Erro (mm)',fontsize = 11)
        ax.set_xlabel('Amostra',fontsize = 11)
        ax.set_title("Erro de Estimativa",fontsize = 12)
        plt.tight_layout()
        plt.scatter(np.arange(0,len(erro_abs_med),1 ),Y_test - y_pred,marker='o')
        plt.grid()
        plt.show()
        fig.savefig(os.path.join(pasta,nome+"Erro.eps"), transparent=True)
        
     
    
        bins = [.75, .80, .85, .90, .95, 1.0, 1.05, 1.1, 1.15, 1.20, 1.25]
        fig = plt.figure(dpi = dpi, figsize = figsize)
        ax = fig.add_subplot(111)
        ax.set_ylabel('Frequência',fontsize = 11)
        ax.set_xlabel('Estimativa/Teste',fontsize = 11)
        ax.set_title("Histograma Estimativa/Teste",fontsize = 12)
        plt.tight_layout()
        _, ymax = ax.get_ylim()
        plt.text(1.1, ymax+400, "$\\mu$ = {:.3f}".format(med_histt),fontsize = 11)
        plt.text(1.1, ymax+350, "$\\sigma$ = {:.3f}".format(dev_histt),fontsize = 11)
        utils.histograma(histt,bins,ax)
        fig.savefig(os.path.join(pasta,nome+" Relação PrediçãoTeste.eps"), transparent=True)
      
        
        histt1 = Y_test - y_pred
        med_histt = np.mean(histt1)
        dev_histt = np.std(histt1)
        
        
        bins = [-1.0,-.9,-.8,-.7,-.6,-.5,-.4,-.3,-.2,-.1,0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1.0]
        bins = [-.5,-.4,-.3,-.2,-.1,0,.1,.2,.3,.4,.5]
        fig = plt.figure(dpi = dpi, figsize = figsize)
        ax = fig.add_subplot(111)
        ax.set_ylabel('Frequência',fontsize = 11)
        ax.set_xlabel('Erro (mm)',fontsize = 11)
        ax.set_title("Histograma Erro de Estimativa",fontsize = 12)
        plt.tight_layout()
        _, ymax = ax.get_ylim()
        plt.text(.15, ymax+400,"$\mu$ = {:.2e}".format(med_histt),fontsize = 11)
        plt.text(.15, ymax+350, "$\\sigma$ = {:.2e}".format(dev_histt),fontsize = 11)
        utils.histograma(histt1,bins,ax)
        fig.savefig(os.path.join(pasta,nome+"Histograma_erro.eps"), transparent=True)
    
    # Salvando os resultados
    dat = pd.DataFrame([mse, R2_lista, erro_max_lista, erro_medio_lista])
    dat.index = linhas
    dat.columns = nomes
    dat.to_excel(os.path.join(pasta,pt1+pt2+'.xlsx'),float_format = '%.5e')
    
if __name__=="__main__":
    main()